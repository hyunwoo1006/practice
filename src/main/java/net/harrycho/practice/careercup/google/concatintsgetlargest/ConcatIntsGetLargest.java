package net.harrycho.practice.careercup.google.concatintsgetlargest;


import java.util.Arrays;
import java.util.Comparator;

/*
    Given a list of integers, find the highest value obtainable by concatenating these together.

    For example, given 9, 918, 917 - The answer is 9918917.
    But given 1,112,113 - The answer is 1131121

 */
public class ConcatIntsGetLargest {

    public static String getLargest(int[] intArr) {


        String[] ints = new String[intArr.length];
        for(int i = 0; i < intArr.length; i++) {
            ints[i] = String.valueOf(intArr[i]);
        }


        Arrays.sort(ints, new Comparator<String>() {
            public int compare(String o1, String o2) {
                char[] str1 = o1.toCharArray();
                char[] str2 = o2.toCharArray();

                int n = Math.max(str1.length, str2.length);

                for(int i = 0 ; i < n; i++) {
                    char digit1 = 99;
                    char digit2 = 99;

                    if(i < str1.length) {
                        digit1 = str1[i];
                    }
                    if(i < str2.length) {
                        digit2 = str2[i];
                    }
                    if(digit1 > digit2) {
                        return -1;
                    }
                    if(digit1 < digit2) {
                        return 1;
                    }
                }
                return 0;
            }
        });



        return String.join("", ints);
    }


    public static void main(String [] args) {


        int[] arg1 = {9, 90};
        System.out.println(getLargest(arg1));

        int[] arg2 = {824, 8247};
        System.out.println(getLargest(arg2));


    }





}

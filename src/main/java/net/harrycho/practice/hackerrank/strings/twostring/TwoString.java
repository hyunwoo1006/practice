package net.harrycho.practice.hackerrank.strings.twostring;

import java.util.HashSet;
import java.util.Scanner;
import java.util.Set;

public class TwoString {

    public static void containsSubstring(String a, String b) {

        char[] aStr = a.toCharArray();

        Set<Character> aChars = new HashSet<Character>();

        for(int i = 0; i < aStr.length; i++) {
            aChars.add(aStr[i]);
        }

        for(int i = 0; i < b.length(); i++) {
            if(aChars.contains(b.charAt(i))){
                System.out.println("YES");
                return;
            }
        }
        System.out.println("NO");
    }


    public static void main(String [] args) {

        Scanner in = new Scanner(System.in);

        int testCount = in.nextInt();

        for(int i = 0; i < testCount; i++) {

            containsSubstring(in.next(), in.next());

        }




    }

}

package net.harrycho.practice.hackerrank.bitwise.sansaxor;

public class SansaXor {

    public static void main(String [] args) {


        int[] arr = {4, 5, 7, 5};

        System.out.println(xored(arr));
    }


    public static int xoredNaive(int[] arr) {
        int result = 0;
        for(int size = 1; size <= arr.length; size++) {
            for(int countInSize = 0; countInSize <= arr.length-size; countInSize++) {
                for(int i = 0; i < size; i++) {
                    result = result ^ arr[i+countInSize];
                }
            }
        }
        return result;
    }

    public static int xored(int [] arr) {
        int result = 0;
        if(arr.length % 2 == 0) {
            return 0;
        } else {
            for(int i = 0, j = 0; i < arr.length/2 + 1; i++, j+=2) {
                result = result ^ arr[j];
            }
        }
        return result;
    }

}

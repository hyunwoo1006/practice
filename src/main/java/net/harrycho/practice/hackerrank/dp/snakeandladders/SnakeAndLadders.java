package net.harrycho.practice.hackerrank.dp.snakeandladders;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;
import java.util.stream.IntStream;

public class SnakeAndLadders {


    public static void getMinEnd(int[] board,
                                Map<Integer, Integer> edges,
                                int steps,
                                int stepsCount,
                                int i) {

        if(i > board.length-1 || board[i] < steps) {
            return;
        }

        if(stepsCount-1 % 6 == 0) {
            steps++;
        }

        board[i] = steps;

        for(int j = i; j < board.length; j++) {

            if(edges.containsKey(j)) {
                getMinEnd(board, edges, steps, stepsCount, edges.get(j));
            }
            getMinEnd(board, edges, steps, stepsCount+1, i+1);
        }

    }

    public static int getMinEnd(int[] board,
                                 Map<Integer, Integer> edges) {


        getMinEnd(board, edges, 1, 1, 1);


        return board[100];
    }


    public static void main(String [] args) {

        Scanner in = new Scanner(System.in);

        int testCount = in.nextInt();
        for(int j = 0; j < testCount; j++){

            int ladderCount = in.nextInt();
            Map<Integer, Integer> edges = new HashMap<Integer, Integer>();
            for(int i = 0; i < ladderCount; i++) {
                edges.put(in.nextInt(), in.nextInt());
            }

            int snakeCount = in.nextInt();
            for(int i = 0; i < snakeCount; i++) {
                edges.put(in.nextInt(), in.nextInt());
            }

            // index 0 is not used; only used indices are 1-100
            // All initialized to Integer.MAX_VALUE, meaning infinite distance
            int[] board = new int[101];
            Arrays.fill(board, Integer.MAX_VALUE);

            System.out.println(getMinEnd(board, edges));

        }

    }


}

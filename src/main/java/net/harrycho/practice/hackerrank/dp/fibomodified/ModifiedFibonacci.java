package net.harrycho.practice.hackerrank.dp.fibomodified;


import java.math.BigInteger;
import java.util.Scanner;

public class ModifiedFibonacci {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        BigInteger t0 = new BigInteger(in.next());
        BigInteger t1 = new BigInteger(in.next());
        int indexToFind = in.nextInt();

        // We don't use 0 index to simplify indexing... Fibo starts from 1 where array starts from 0
        BigInteger[] solved = new BigInteger[indexToFind + 1];

        solved[1] = t0;
        solved[2] = t1;

        for(int i = 3; i <= indexToFind; i++) {
            solved[i] = solved[i-1].multiply(solved[i-1]).add(solved[i-2]);
        }
        System.out.println(solved[indexToFind]);
    }
}

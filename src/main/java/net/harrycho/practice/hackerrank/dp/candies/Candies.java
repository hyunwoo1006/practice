package net.harrycho.practice.hackerrank.dp.candies;

import org.apache.commons.lang3.ArrayUtils;

import java.util.Arrays;
import java.util.Scanner;
import java.util.stream.IntStream;

public class Candies {

    public static void main(String [] args) {

        Scanner in = new Scanner(System.in);

        int count = in.nextInt();
        int[] ratings = new int[count];
        int[] candies = new int[count];

        for(int i = 0; i < count; i++) {
            ratings[i] = in.nextInt();
        }

        System.out.println(distribute(ratings, candies));

    }


    public static int distribute(int[] ratings, int[] candies) {

        candies[0] = 1;
        for(int i = 1; i < ratings.length; i++) {

            if(ratings[i] > ratings[i-1]) {
                candies[i] = candies[i-1] + 1;
            } else if(ratings[i] < ratings[i-1]) {
                candies[i] = 1;
                if(candies[i-1] == 1) {
                    for (int j = i-1; j >= 0; j--) {
                        candies[j] = candies[j] + 1;
                        if ((j - 1 >= 0) && ((ratings[j] > ratings[j - 1]) || (ratings[j] < ratings[j-1] && candies[j] < candies[j-1]))) {
                            break;
                        }
                    }
                }
            } else {
                if(ratings[i-1] == 1) {
                    candies[i] = 2;
                } else {
                    candies[i] = 1;
                }
            }

        }

        System.out.println(Arrays.asList(ArrayUtils.toObject(candies)));
        return Arrays.stream(candies).sum();

    }
}

package net.harrycho.practice.hackerrank.dp.coinchange;

import java.util.Arrays;
import java.util.NavigableMap;
import java.util.Scanner;

public class CoinChange {

    public static void main(String [] args) {

        Scanner in = new Scanner(System.in);

        int N = in.nextInt();
        int size = in.nextInt();

        int[] coins = new int[size];
        for(int i = 0; i < coins.length; i++) {
            coins[i] = in.nextInt();
        }

        long[][] memo = new long[N+1][coins.length];
        for(int i = 0; i < memo.length; i++) {
            Arrays.fill(memo[i], -1);
        }

        System.out.println(count(memo, coins, N));

    }




    public static long count(long[][] memo, int[] coins, int N) {
        return count(memo, coins, N, coins.length-1);
    }


    public static long count(long[][] memo, int[] coins, int N, int i) {

        if( N == 0 ) {
            return 1;
        } else if( N < 0 || i < 0) {
            return 0;
        } else {
            if(memo[N][i] == -1) {
                // Solve
                memo[N][i] = count(memo, coins, N, i - 1) + count(memo, coins, N - coins[i], i);
                return memo[N][i];
            } else  {
                // Solved already
                return memo[N][i];
            }
        }

    }
}

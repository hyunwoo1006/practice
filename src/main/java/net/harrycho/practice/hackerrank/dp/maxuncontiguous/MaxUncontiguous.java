package net.harrycho.practice.hackerrank.dp.maxuncontiguous;

import java.util.Arrays;

public class MaxUncontiguous {

    public static int maxUncontiguous(int[] arr) {

        int max = arr[0];
        for(int i = 1; i < arr.length; i++) {
            if(max < 0) {
                max = Math.max(max, arr[i]);
            } else {
                max = Math.max(max, max + arr[i]);
            }
        }
        return max;
    }

    public static void main(String [] args) {


        int[] testdata1 = {2, -5, 3, 4, -7, 8};
        System.out.println(maxUncontiguous(testdata1));

        int[] testdata2 = {1, 2, 3, 4};
        System.out.println(maxUncontiguous(testdata2));

        int[] testdata3 = {2, -1, 2, 3, 4, -5};
        System.out.println(maxUncontiguous(testdata3));

        int[] testdata4 = {-1, -2, -3, -4, -5};
        System.out.println(maxUncontiguous(testdata4));



    }

}

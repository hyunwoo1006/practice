package net.harrycho.practice.hackerrank.dp.maxcontiguous;

import java.util.Arrays;

public class MaxContiguous {

    public static int maxContiguous(int[] arr) {

        int maxEnding = arr[0];
        int maxSoFar = arr[0];
        for(int i = 1; i < arr.length; i++) {

            maxEnding = Math.max(arr[i], maxEnding + arr[i]);
            maxSoFar = Math.max(maxEnding, maxSoFar);
            System.out.println(maxEnding + " " + maxSoFar);
        }
        return maxSoFar;
    }

    public static void main(String [] args) {

        int[] testdata1 = {2, -5, 3, 4, -7, 8};
        System.out.println(maxContiguous(testdata1));
//
//        int[] testdata2 = {1, 2, 3, 4};
//        System.out.println(maxContiguous(testdata2));
//
//        int[] testdata3 = {2, -1, 2, 3, 4, -5};
//        System.out.println(maxContiguous(testdata3));
//
//        int[] testdata4 = {-1, -2, -3, -4, -5};
//        System.out.println(maxContiguous(testdata4));


    }
}

package net.harrycho.practice.hackerrank.arrays.TwoDArrayDS;

import java.util.Scanner;

/**
 * Created by root on 11/20/15.
 */
public class Solution {

    private static final int SIZE = 6;
    private static Scanner in = new Scanner(System.in);


    public static void main(String [] args) {
        int[][] matrix = new int[SIZE][SIZE];



        for(int i = 0; i < matrix.length; i++) {
            for(int j = 0; j < matrix.length; j++) {
                matrix[i][j] = Integer.parseInt(in.next());
            }
        }



        int max = Integer.MIN_VALUE;

        for(int i = 0; i < matrix.length - 2; i++) {
            for(int j = 0; j < matrix.length - 2; j++) {


                int sum = matrix[i][j] + matrix[i][j+1] + matrix[i][j+2]
                        + matrix[i+1][j+1]
                        + matrix[i+2][j] + matrix[i+2][j+1] + matrix[i+2][j+2];

                if(sum > max) {
                    max = sum;
                }

            }
        }


        System.out.println(max);
    }


}

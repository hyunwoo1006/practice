package net.harrycho.practice.hackerrank.arrays.ArrayDS;

import java.util.Scanner;

public class Solution {

    public static void main(String [] args) {

        Scanner in = new Scanner(System.in);

        int inputCount = Integer.parseInt(in.next());

        String printStr = "";

        for(int i = 0; i < inputCount; i++) {
            int num = Integer.parseInt(in.next());


            printStr = num + " " + printStr;
        }

        System.out.print(printStr);
    }

}

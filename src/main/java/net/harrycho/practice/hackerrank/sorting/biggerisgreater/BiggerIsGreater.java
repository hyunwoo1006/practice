package net.harrycho.practice.hackerrank.sorting.biggerisgreater;

import java.util.Arrays;
import java.util.Scanner;

public class BiggerIsGreater {


    public static String biggerIsGreater(String str) {

        char[] charStr = str.toCharArray();
        if(!isSolvable(charStr)) {
            return "no answer";
        }

        // going from left to right;
        int leftCharIndex = charStr.length-2;
        for(int i = charStr.length - 1; i > 0; i--) {

            if(charStr[i-1] < charStr[i]) {
                leftCharIndex = i-1;
                break;
            }
        }

        int nextGreaterThanLeftChar = leftCharIndex + 1;
        for(int i = nextGreaterThanLeftChar; i < charStr.length; i++) {
            if(charStr[i] > charStr[leftCharIndex] && charStr[i] < charStr[nextGreaterThanLeftChar]) {
                nextGreaterThanLeftChar = i;
            }
        }

        swap(charStr, leftCharIndex, nextGreaterThanLeftChar);
        Arrays.sort(charStr, leftCharIndex+1, charStr.length);

        return new String(charStr);

    }

    public static void swap(char[] arr, int i, int j) {

        char temp = arr[i];
        arr[i] = arr[j];
        arr[j] = temp;

    }

    public static boolean isSolvable(char[] charStr) {
        // if everything is sorted in greater -> smallest || all same chars, then no answer

        if(charStr.length == 1) {
            return false;
        }
        boolean allIncreasing = true;
        boolean allEqual = true;
        for(int i = 1; i < charStr.length; i++) {
            if(charStr[i-1] < charStr[i]) {
                allIncreasing = false;
            }

            if(charStr[i-1] != charStr[i]) {
                allEqual = false;
            }

        }
        return !allIncreasing && !allEqual;
    }


    public static void main(String [] args) {

        Scanner in = new Scanner(System.in);
        int testCount = in.nextInt();
        for(int i = 0; i < testCount; i++) {
            System.out.println(biggerIsGreater(in.next()));


        }

    }



}

package net.harrycho.practice.hackerrank.sorting.fullcountingsort;

import java.util.*;

public class FullCountingSort {

    public static void main(String [] args) {

        Map<Integer, StringBuilder> map = new HashMap<Integer,StringBuilder>();

        Scanner in = new Scanner(System.in);

        int inputCount = in.nextInt();
        for(int i = 0; i < inputCount; i++) {

            int intVal = in.nextInt();
            String str = in.next();

            if(i < inputCount / 2) {
                str = "-";
            }

            if(map.containsKey(intVal)) {
                map.get(intVal).append(" " + str);
            } else {
                map.put(intVal, new StringBuilder(str));
            }

        }

        List<Integer> keyList = new ArrayList<Integer>(map.keySet());
        Collections.sort(keyList);


        StringBuilder finalStr = new StringBuilder();
        for(int i = 0; i < keyList.size(); i++) {
            finalStr.append(map.get(keyList.get(i)) + " ");
        }

        System.out.println(finalStr.toString());

    }
}

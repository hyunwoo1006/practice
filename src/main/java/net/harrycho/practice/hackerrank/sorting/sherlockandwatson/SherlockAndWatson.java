package net.harrycho.practice.hackerrank.sorting.sherlockandwatson;

import java.util.LinkedList;
import java.util.Scanner;

public class SherlockAndWatson {

    private LinkedList<Integer>[] adj;

    public static void main(String[] args) {


        Scanner in = new Scanner(System.in);

        int numOfInts = in.nextInt();
        int numOfRots = in.nextInt();
        int numOfQueries = in.nextInt();

        int[] data = new int[numOfInts];

        for(int i = 0; i < numOfInts; i++) {
            data[i] = in.nextInt();
        }


        numOfRots %= numOfInts;

        for(int i = 0; i < numOfQueries; i++) {

            int qIndex = in.nextInt();

            qIndex = qIndex-numOfRots < 0 ? qIndex-numOfRots+numOfInts : qIndex-numOfRots;
            System.out.println(data[qIndex]);

        }
    }

}

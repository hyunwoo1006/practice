package net.harrycho.practice.hackerrank.datastructure.doublylinkedlist.InsertToSortedList;

import net.harrycho.practice.commons.list.DLNode;

public class Driver {
    public static void main(String [] args) {

        DLNode head = new DLNode(2);

        DLNode node1 = new DLNode(4);

        DLNode node2 = new DLNode(6);

        head.next = node1;
        node1.prev = head;
        node1.next = node2;
        node2.prev = node1;


        SortedInsert sortedInsert = new SortedInsert();
        sortedInsert.sortedInsert(head, 5);


        printDLList(head);


    }

    public static void printDLList(DLNode head) {

        DLNode curr = head;
        while(curr != null) {
            System.out.println("prev=" + (curr.prev == null ? null : curr.prev.data) +
                    " data=" + curr.data +
                    " next=" + (curr.next == null ? null : curr.next.data));
            curr = curr.next;

        }

    }

}

package net.harrycho.practice.hackerrank.datastructure.singlylinkedlist.mergedsorted;

import net.harrycho.practice.commons.list.SLNode;

public class MergeSortedList {

    public SLNode MergeLists(SLNode headA, SLNode headB) {

        if(headA == null && headB == null) {
            return null;
        } else if(headA == null) {
            return headB;
        } else if(headB == null) {
            return headA;
        }

        // Now both are not null

        // Find and create the new head
        SLNode head = null;
        if(headA.val < headB.val) {
            head = new SLNode();
            head.val = headA.val;
            headA = headA.next;
        } else {
            head = new SLNode();
            head.val = headB.val;
            headB = headB.next;
        }

        SLNode curr = head;
        while(headA != null || headB != null) {

            int dataA = Integer.MAX_VALUE;
            int dataB = Integer.MAX_VALUE;

            if(headA != null) {
                dataA = headA.val;
            }

            if(headB != null) {
                dataB = headB.val;
            }

            SLNode newNode = new SLNode();
            if(dataA <= dataB) {
                newNode.val = dataA;
                headA = headA.next;

            } else {
                newNode.val = dataB;
                headB = headB.next;
            }
            curr.next = newNode;
            curr = curr.next;
        }
        return head;
    }

}

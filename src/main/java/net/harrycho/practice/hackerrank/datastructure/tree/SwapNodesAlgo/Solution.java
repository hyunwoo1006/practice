package net.harrycho.practice.hackerrank.datastructure.tree.SwapNodesAlgo;

import java.util.Scanner;

public class Solution {


    public static void main(String [] args) {

        Scanner in = new Scanner(System.in);
        int nodeCount = Integer.parseInt(in.nextLine());

        int[] left = new int[nodeCount + 1];
        int[] right = new int[nodeCount + 1];
        int[] depth = new int[nodeCount + 1];

        for(int i = 1; i <= nodeCount; i++) {
            left[i] = Integer.parseInt(in.next());
            right[i] = Integer.parseInt(in.next());
        }

        getDepth(left, right, depth, 1, 1);

        int swapCount = Integer.parseInt(in.next());

        for(int i = 0; i < swapCount; i++) {
            int swap = Integer.parseInt(in.next());

            for(int j = 1; j <= nodeCount; j++) {

                if(depth[j] % swap == 0) {

                    int temp = left[j];
                    left[j] = right[j];
                    right[j] = temp;

                }
            }

            printInOrder(left, right, 1);
            System.out.println();

        }
    }

    private static void getDepth(int[] l, int[] r, int[] d,  int curr, int depth) {
        d[curr] = depth;
        if(l[curr] > 0) getDepth(l, r, d, l[curr], depth + 1);
        if(r[curr] > 0) getDepth(l, r, d, r[curr], depth + 1);
    }

    private static void printInOrder(int[] l, int[] r, int curr) {
        if(l[curr] > 0) printInOrder(l, r, l[curr]);
        System.out.print(curr + " ");
        if(r[curr] > 0) printInOrder(l, r, r[curr]);
    }

}

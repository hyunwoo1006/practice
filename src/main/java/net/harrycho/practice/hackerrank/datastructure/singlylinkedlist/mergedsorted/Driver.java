package net.harrycho.practice.hackerrank.datastructure.singlylinkedlist.mergedsorted;

import net.harrycho.practice.commons.list.SLNode;

public class Driver {

    public static void main(String [] args) {

        MergeSortedList mergeSortedList = new MergeSortedList();


        SLNode headA = new SLNode(1);
        SLNode nodeA1 = new SLNode(3);
        SLNode nodeA2 = new SLNode(5);
        SLNode nodeA3 = new SLNode(7);
        headA.next = nodeA1;
        nodeA1.next = nodeA2;
        nodeA2.next = nodeA3;

        SLNode headB = new SLNode(2);
        SLNode nodeB1 = new SLNode(4);
        SLNode nodeB2 = new SLNode(6);
        SLNode nodeB3 = new SLNode(8);
        headB.next = nodeB1;
        nodeB1.next = nodeB2;
        nodeB2.next = nodeB3;


        SLNode merged = mergeSortedList.MergeLists(headA, headB);
        System.out.println("break");

    }
}

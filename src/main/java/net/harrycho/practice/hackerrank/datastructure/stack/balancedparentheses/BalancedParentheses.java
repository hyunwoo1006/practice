package net.harrycho.practice.hackerrank.datastructure.stack.balancedparentheses;

import java.util.Scanner;
import java.util.Stack;

public class BalancedParentheses {


    public static String verifyParentheses(String str) {
        if(!isOpening(str.charAt(0))) return "NO";

        Stack<Character> stack = new Stack<Character>();
        stack.push(str.charAt(0));

        for(int i = 1; i < str.length(); i++) {

            char currChar = str.charAt(i);
            if(isOpening(currChar)) {
                // Add regardless
                stack.push(currChar);
            } else {
                if(stack.empty()) {
                    return "NO";
                } else {
                    char lastExpected = stack.peek();
                    if(!isOpening(lastExpected)) {
                        return "NO";
                    } else {
                        char getMatchedClosing = getClosing(lastExpected);
                        if(getMatchedClosing == currChar) {
                            stack.pop();
                        } else {
                            return "NO";
                        }
                    }
                }

            }
        }
        if(stack.empty()) {
            return "YES";
        } else {
            return "NO";
        }
    }


    public static void main(String [] args) {

        Scanner in = new Scanner(System.in);

        int testCount = in.nextInt();

        for(int i = 0; i < testCount; i++) {
            System.out.println(verifyParentheses(in.next()));
        }

    }
    public static  boolean isOpening(char ch) {
        /**
         * Simpler Approach
         *
         * String opening = "([{";
         * if(opening.contains(ch)) return true;
         * return false;
         *
         */


        if(ch == '(' || ch == '[' || ch == '{') {
            return true;
        } else {
            return false;
        }
    }

    public static char getClosing(char opening) {
        switch(opening) {
            case '(' : return ')';
            case '[' : return ']';
            default  : return '}';
        }
    }

}

package net.harrycho.practice.hackerrank.datastructure.doublylinkedlist.InsertToSortedList;

import net.harrycho.practice.commons.list.DLNode;

public class SortedInsert {

    public DLNode sortedInsert(DLNode head, int data) {

        DLNode newNode = new DLNode();
        newNode.data = data;

        if(head == null) {
            return newNode;
        } else if(head.data >= data) {
            newNode.next = head;
            head.prev = newNode;
            return newNode;
        } else {

            DLNode curr = head.next;
            DLNode prev = head;
            while(curr != null) {

                if(curr.data >= data) {

                    newNode.prev = curr.prev;
                    newNode.next = curr;
                    curr.prev.next = newNode;
                    curr.prev = newNode;

                    return head;

                }

                prev = curr;
                curr = curr.next;
            }

            newNode.prev = prev;
            prev.next = newNode;
            return head;

        }

    }

}

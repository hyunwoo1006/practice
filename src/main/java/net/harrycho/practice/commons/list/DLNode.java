package net.harrycho.practice.commons.list;

public class DLNode {

    public int data;
    public DLNode next;
    public DLNode prev;

    public DLNode() {

    }

    public DLNode(int data) {
        this.data = data;
    }

}

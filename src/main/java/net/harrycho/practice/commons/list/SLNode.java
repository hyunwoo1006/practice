package net.harrycho.practice.commons.list;

public class SLNode {
    public SLNode next;
    public int val;

    public SLNode() {
    }

    public SLNode(int val) {
        this.val = val;
    }

    public SLNode(SLNode next, int val) {
        this.next = next;
        this.val = val;
    }
}

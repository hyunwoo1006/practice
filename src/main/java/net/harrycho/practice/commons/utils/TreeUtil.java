package net.harrycho.practice.commons.utils;

import net.harrycho.practice.commons.tree.TreeNode;

import java.util.LinkedList;
import java.util.Queue;

/**
 * Created by root on 12/3/15.
 */
public class TreeUtil {
    private TreeUtil() {
        // None
    }

    public static void printTree(TreeNode root) {
        // In order to print a tree, level is needed






    }

    public static int getLevel(TreeNode root) {
        if(root == null) {
            return 0;
        }

        Queue<TreeNode> q = new LinkedList<TreeNode>();
        q.add(root);

        int level = 0;

        while(!q.isEmpty()) {

            int qSize = q.size();

            for(int i = 0; i < qSize; i++) {
                TreeNode n = q.remove();

                if(n.left != null) {
                    q.add(n.left);
                }

                if(n.right != null) {
                    q.add(n.right);
                }
            }

            level++;

        }

        return level;

    }

    public static boolean treeEquals(TreeNode t1, TreeNode t2) {

        // Checks if two trees are identical by value (not the reference)
        if(t1 == null && t2 == null) {
            return true;
        } else {
            if(t1 == null) {
                return false;
            }
            if(t2 == null) {
                return false;
            }
        }

        Queue<TreeNode> t1q = new LinkedList<TreeNode>();
        Queue<TreeNode> t2q = new LinkedList<TreeNode>();

        while(!t1q.isEmpty() || !t2q.isEmpty()) {

            if(t1q.size() != t2q.size()) {
                return false;
            }
            for(int i = 0; i < t1q.size(); i++) {
                TreeNode t1TreeNode = t1q.remove();
                TreeNode t2TreeNode = t2q.remove();

                if(t1TreeNode.value != t2TreeNode.value) {
                    return false;
                }

                if(t1TreeNode.left != null) {
                    t1q.add(t1TreeNode.left);
                }

                if(t1TreeNode.right != null) {
                    t1q.add(t1TreeNode.right);
                }

                if(t2TreeNode.left != null) {
                    t2q.add(t2TreeNode.left);
                }

                if(t2TreeNode.right != null) {
                    t2q.add(t2TreeNode.right);
                }

            }
        }
        return true;
    }

    public static void main(String [] args) {

        // Testing tree utils.        // Build tree bottom-up
        TreeNode a10 = new TreeNode(10);
        TreeNode a1 = new TreeNode(1);
        TreeNode a20 = new TreeNode(a10, a1, 20);
        TreeNode a30 = new TreeNode(30);
        TreeNode a60 = new TreeNode(a30, a20, 60);
        TreeNode a40 = new TreeNode(40);
        TreeNode a25 = new TreeNode(25);
        TreeNode a50 = new TreeNode(a40, a25, 50);
        TreeNode aroot = new TreeNode(a60, a50, 90);
        System.out.println(getLevel(aroot));

        TreeNode b10 = new TreeNode(10);
        TreeNode b1 = new TreeNode(1);
        TreeNode b30 = new TreeNode(b10, b1, 30);
        TreeNode b20 = new TreeNode(20);
        TreeNode b60 = new TreeNode(b30, b20, 60);
        TreeNode b40 = new TreeNode(40);
        TreeNode b25 = new TreeNode(25);
        TreeNode b50 = new TreeNode(b40, b25, 50);
        TreeNode broot = new TreeNode(b60, b50, 90);
        System.out.println(getLevel(broot));

        TreeNode c5 = new TreeNode(5);
        TreeNode croot = new TreeNode(c5, null, 10);
        System.out.println(getLevel(croot));

        TreeNode d5 = new TreeNode(5);
        TreeNode droot = new TreeNode(null, d5, 10);
        System.out.println(getLevel(droot));

        TreeNode e3 = new TreeNode(3);
        TreeNode e6 = new TreeNode(6);
        TreeNode e5 = new TreeNode(e3, e6, 5);
        TreeNode eroot = new TreeNode(e5, null, 10);
        System.out.println(getLevel(eroot));

        TreeNode f3 = new TreeNode(3);
        TreeNode f4 = new TreeNode(4);
        TreeNode f5 = new TreeNode(f3, f4, 5);
        TreeNode froot = new TreeNode(f5, null, 10);
        System.out.println(getLevel(froot));

        TreeNode groot = new TreeNode(null, null, 1);
        System.out.println(getLevel(groot));

    }
}

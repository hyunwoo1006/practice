package net.harrycho.practice.random.strings.subsequences;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class Subsequences {


    public static void main(String [] args) {

        String input = "abcd";

        List<String> str = subsequences(input);
        str.add(input);
        for(String s : str) {
            System.out.println(s);
        }

    }

    public static List<String> subsequences(String str) {

        List<String> subsequences = new ArrayList<String>();
        subsequences(subsequences, "", str);


        Collections.sort(subsequences, new Comparator<String>() {
            public int compare(String o1, String o2) {
                if(o1.length() < o2.length()) {
                    return -1;
                } else if(o1.length() > o2.length()) {
                    return 1;
                } else {
                    return o1.compareTo(o2);
                }


            }
        });
        return subsequences;
    }


    public static void subsequences(List<String> subsequences, String prefix, String remaining) {

        if(remaining.length() == 0) {
            return;
        }

        subsequences.add(prefix);
        for(int i = 0; i < remaining.length(); i++) {
            subsequences(subsequences, prefix+remaining.charAt(i), removeChar(remaining, i));
        }



    }


    public static String removeChar(String str, int i) {
        if(i == 0) {
            return str.substring(1, str.length());
        } else {
            return str.substring(0, i) + str.substring(i+1, str.length());
        }
    }

}

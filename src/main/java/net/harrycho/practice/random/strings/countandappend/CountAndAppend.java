package net.harrycho.practice.random.strings.countandappend;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

public class CountAndAppend {

    public static String countAndAppend(String str) {


        Map<Character, Integer> counts = new LinkedHashMap<Character, Integer>();
        for(int i = 0; i < str.length(); i++) {
            char ch = str.charAt(i);
            if(!counts.containsKey(ch)) {
                counts.put(str.charAt(i), 1);
            } else {
                counts.put(ch, counts.get(ch)+1);
            }
        }

        StringBuilder sb = new StringBuilder();
        for(char ch : counts.keySet()) {
            sb.append(counts.get(ch));
            sb.append(ch);
        }
        return sb.toString();
    }


    public static void main(String [] args) {

        String inputStr = "1";
        int applyCount = 5;

        String applied = inputStr;
        for(int i = 0; i < applyCount; i++) {
            System.out.println(applied);
            applied = countAndAppend(applied);
        }

        System.out.println(applied);
    }

}

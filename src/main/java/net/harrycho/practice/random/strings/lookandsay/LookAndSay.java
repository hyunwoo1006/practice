package net.harrycho.practice.random.strings.lookandsay;

public class LookAndSay {


    public static String lookAndSay(String str) {

        char currChar = str.charAt(0);
        int currCount = 1;
        StringBuilder sb = new StringBuilder();
        for(int i = 1; i < str.length(); i++) {
            if(str.charAt(i) != currChar) {
                sb.append(currCount);
                sb.append(currChar);
                currChar = str.charAt(i);
                currCount = 1;
            } else {
                currCount++;
            }

        }
        sb.append(currCount);
        sb.append(currChar);
        return sb.toString();
    }

    public static void main(String [] args) {

        String inputStr = "1";
        int applyCount = 8;

        String currStr = inputStr;
        for(int i = 0; i <= applyCount; i++) {
            System.out.println(currStr);
            currStr = lookAndSay(currStr);
        }




    }






}

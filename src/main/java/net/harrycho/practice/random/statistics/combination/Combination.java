package net.harrycho.practice.random.statistics.combination;

import java.util.ArrayList;
import java.util.List;

public class Combination {

    public static List<String> getCombinations(List<Character> charStr, int size) {
        List<String> combs = new ArrayList<String>();
        if(size > 0) {
            getCombinations(combs, charStr, new StringBuilder(), size, 0);
        }
        return combs;
    }

    public static void getCombinations(List<String> combs, List<Character> charStr, StringBuilder prefix, int size, int i) {

        if(prefix.length()+1 == size) {
            for(int j = i; j < charStr.size(); j++) {
                combs.add(prefix.append(charStr.get(j)).toString());
                prefix.deleteCharAt(prefix.length()-1);
            }
        } else {
            for(int j = i; j <= charStr.size()-size+i; j++) {
                if(j < charStr.size()) {
                    getCombinations(combs, charStr, prefix.append(charStr.get(j)), size, j+1);
                    prefix.deleteCharAt(prefix.length()-1);
                }
            }
        }

    }

    public static void main(String [] args) {

        String str = "1";
        List<Character> chars = new ArrayList<Character>();
        for(char ch : str.toCharArray()) {
            chars.add(ch);
        }

        for(int i = 1; i <= str.length(); i++) {
            System.out.println(getCombinations(chars, i));
        }

    }

}

package net.harrycho.practice.random.statistics.permutation;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Permutation {


    public static void getAllPermutation(List<String> perms, List<Character> remaining, StringBuilder prefix) {

        if(remaining.size() == 0) {
            // If we are checking from anagram this is the point where anagram exists from dictionary or not
//            perms.add(prefix.toString());
//            System.out.println(prefix.toString());
        } else {
            for(int i = 0; i < remaining.size(); i++) {
                char ch = remaining.remove(i);
                // As for checking anagram, we can check if prefix exists in dictionary to eliminate recursing useless parts

                getAllPermutation(perms, remaining, prefix.append(ch));
                remaining.add(i, ch);
                prefix.deleteCharAt(prefix.length()-1);
            }
        }

    }

    public static List<String> getAllPermutation(String str) {

        List<String> perms = new ArrayList<String>();
        List<Character> strChars = new ArrayList<Character>();
        for(char ch : str.toCharArray()) {
            strChars.add(ch);
        }

        getAllPermutation(perms, strChars, new StringBuilder(""));
        return perms;
    }

    public static void getAllPermutationWithString(List<String> perms, List<Character> remaining, String prefix) {

        if (remaining.size() == 0) {
            // If we are checking from anagram this is the point where anagram exists from dictionary or not
//            perms.add(prefix.toString());
//            System.out.println(prefix);
        } else {
            for (int i = 0; i < remaining.size(); i++) {
                char ch = remaining.remove(i);
                String newPrefix = prefix + ch;

                // As for checking anagram, we can check if prefix exists in dictionary to eliminate recursing useless parts

                getAllPermutationWithString(perms, remaining, newPrefix);
                remaining.add(i, ch);
            }
        }
    }


    public static List<String> getAllPermutationWithString(String str) {


        List<String> perms = new ArrayList<String>();
        List<Character> strChars = new ArrayList<Character>();
        for(char ch : str.toCharArray()) {
            strChars.add(ch);
        }

        getAllPermutationWithString(perms, strChars, "");
        return perms;



    }

    public static void main(String [] args) {
        String testString = "abcdefghijk";

        for(int i = 0; i < 3; i++) {
            long startTime1 = System.currentTimeMillis();
            getAllPermutation(testString);
            long endTime1 = System.currentTimeMillis();

            long startTime2 = System.currentTimeMillis();
            getAllPermutationWithString(testString);
            long endTime2 = System.currentTimeMillis();



            System.out.println("good:" + (endTime1 - startTime1));
            System.out.println("bad:" + (endTime2 - startTime2));

        }
    }


}

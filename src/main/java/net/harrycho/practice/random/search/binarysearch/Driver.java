package net.harrycho.practice.random.search.binarysearch;

public class Driver {
    public static void main(String [] args) {

        BinarySearch bsearch = new BinarySearch();

        int[] testdata1 = {1};
        int[] testdata2 = {1, 2};
        int[] testdata3 = {1, 2, 3};

        System.out.println(bsearch.binarySearch(testdata1, 1));
        System.out.println(bsearch.binarySearch(testdata2, 1));
        System.out.println(bsearch.binarySearch(testdata2, 2));
        System.out.println(bsearch.binarySearch(testdata3, 3));

    }
}

package net.harrycho.practice.random.search.binarysearch;

public class BinarySearch {
    /**
     * Binary Search on SORTED array
     *
     * @param data
     * @return
     */
    public int binarySearch(int[] data, int target) {

        return binarySearch(data, 0, data.length-1, target);

    }

    /**
     *
     *
     * @param data  data
     * @param left  inclusive
     * @param right inclusive
     * @return
     */
    private int binarySearch(int[] data, int left, int right, int target) {

        if(left > right) {
            return -1;
        }

        int middle = left + (right-left) / 2;

        if(data[middle] == target) {
            return middle;
        } else if(data[middle] < target) {
            return binarySearch(data, middle + 1, right, target);
        } else {
            return binarySearch(data, left, middle - 1, target);
        }

    }

}

package net.harrycho.practice.random.dp.minstepstoone;

public class Driver {

    public static void main(String [] args) {

        MinStepsToOne problem = new MinStepsToOne();
        System.out.println(problem.getMinSteps(1));
        
        System.out.println(problem.getMinSteps(4));
        System.out.println(problem.getMinSteps(7));

    }
}

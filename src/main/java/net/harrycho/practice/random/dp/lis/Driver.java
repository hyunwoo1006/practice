package net.harrycho.practice.random.dp.lis;

public class Driver {
    public static void main(String [] args) {

        LongestIncreasingSubsequence lis = new LongestIncreasingSubsequence();


        int[] input1 = {10, 29, 9, 33, 21, 50, 41, 60, 80};
        int[] input2 = {0, 8, 4, 12, 2, 10, 6, 14, 1, 9, 5, 13, 3, 11, 7, 15, 1};
        int[] input3 = {7, 0, 9, 2, 8, 4, 1};

        System.out.println(lis.lisLength(input1));
        System.out.println(lis.lisLength(input2));
        System.out.println(lis.lisLength(input3));

    }
}

package net.harrycho.practice.random.dp.lis;

import org.apache.commons.lang3.ArrayUtils;

import java.util.Arrays;
import java.util.Collections;

public class LongestIncreasingSubsequence {


    public int lisLength(int[] arr) {

        int[] lis = new int[arr.length];

        for(int i = 0; i < lis.length; i++) {
            lis[i] = 1;
        }

        for(int i = 1; i < arr.length; i++) {

            int newComing = arr[i];
            int largestCount = 0;

            for(int j = 0; j < i; j++) {

                if(arr[j] < newComing && largestCount < lis[j]) {
                    largestCount = lis[j];
                }


            }
            lis[i] = largestCount + 1;

        }

        return Collections.max(Arrays.asList(ArrayUtils.toObject(lis)));

    }

    public int[] lis(int[] arr) {
        return null;
    }




}

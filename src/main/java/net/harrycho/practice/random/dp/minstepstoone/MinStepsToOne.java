package net.harrycho.practice.random.dp.minstepstoone;

// Given a number n, you can do three operations:
// 1) n - 1
// 2) n / 2
// 3) n / 3
// Problem : Find a min steps to get to 1
// Eg: for n = 7, ans=3 because (7-1) = 6, 6/3 = 2, 2-1=1 or 2/2=1

public class MinStepsToOne {

    private int[] steps = null;

    public int getMinSteps(int n) {
        init(n);

        for(int i = 2; i <= n; i++) {

            steps[i] = 1 + steps[i-1];
            if(i%2 == 0) {
                steps[i] = Math.min(steps[i], 1 + steps[i/2]);
            }
            if(i%3 ==0) {
                steps[i] = Math.min(steps[i], 1 + steps[i/3]);
            }
        }
        return steps[n];

    }


    private void init(int n) {
        steps = new int[n+1];
        for(int i = 0; i < steps.length; i++) {
            steps[i] = n-1; // Given any n, maximum steps is n-1
        }
        steps[1] = 0;
    }


}

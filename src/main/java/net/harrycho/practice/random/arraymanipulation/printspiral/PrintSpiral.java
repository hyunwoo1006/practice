package net.harrycho.practice.random.arraymanipulation.printspiral;

public class PrintSpiral {

    public static void printSpiral(int[][] data) {

        for(int i = 0; i < data.length / 2; i++) {

            printSpiral(data, i);
        }

        if(data.length % 2 == 1) {
            System.out.print(data[data.length/2][data.length/2]);
        }

        System.out.println();
    }

    /**
     *
     *
     * @param data
     * @param rectangleIndex    0=Outermost
     */
    private static void printSpiral(int[][] data, int rectangleIndex) {

        for(int i = 0; i < data.length-(2*rectangleIndex)-1; i++) {
            System.out.print(data[rectangleIndex][rectangleIndex+i] + " ");
        }

        for(int i = 0; i < data.length-(2*rectangleIndex)-1; i++) {
            System.out.print(data[rectangleIndex+i][data.length-1-rectangleIndex] + " ");
        }

        for(int i = 0; i < data.length-(2*rectangleIndex)-1; i++) {
            System.out.print(data[data.length-1-rectangleIndex][data.length-1-rectangleIndex-i] + " ");
        }

        for(int i = 0; i < data.length-(2*rectangleIndex)-1; i++) {
            System.out.print(data[data.length-1-rectangleIndex-i][rectangleIndex] + " ");
        }
    }

    public static void main(String [] args) {
        int[][] testData1 = {{1, 2, 3},
                             {4, 5, 6},
                             {7, 8, 9}};

        printSpiral(testData1);

        int[][] testData2 = {{1, 2},
                             {4, 5}};

        printSpiral(testData2);

        int[][] testData3 = {{10, 11, 12, 13},
                             {14, 15, 16, 17},
                             {18, 19, 20, 21},
                             {22, 23, 24, 25}};
        printSpiral(testData3);
    }



}

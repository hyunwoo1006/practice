package net.harrycho.practice.random.sorting.mergesort;

import org.apache.commons.lang3.ArrayUtils;

import java.util.Arrays;
import java.util.Random;

public class Driver {

    private static final int INT_RANGE       = 300000000;
    private static final int ARR_SIZE        = 33554432;  // 2^25
    private static final int TEST_COUNT      = 5;
    private static final int[] THREAD_COUNTS = {2, 4, 8, 16, 32, 64, 128, 256, 512, 1024, 2048, 4096};

    public static void main(String [] args) {
        long initialMemory = Runtime.getRuntime().totalMemory();

        long[][] testResults = new long[THREAD_COUNTS.length+2][TEST_COUNT];

        /**
         * For every test....
         *
         * 1. Java's native sort. It uses quick sort
         * 2. Parallel for each given thread count
         *
         */
//        for(int i = 0; i < testResults.length; i++) {
//
//            for (int testCounts = 0; testCounts < TEST_COUNT; testCounts++) {
//
//                // Prepare dataset.
//                int[][] dataset = new int[testResults.length][0];
//                int[] randomInts = generateRandomArray(ARR_SIZE, INT_RANGE);
//                for(int j = 0; j < testResults.length; j++) {
//                    // Copy generated array into others for accurate comparison
//                    dataset[j] = Arrays.copyOf(randomInts, ARR_SIZE);
//                }
//
//                long sTimeJavaNative = System.currentTimeMillis();
//                Arrays.sort(dataset[0]);
//                long eTimeJavaNative = System.currentTimeMillis();
//                testResults[0][testCounts] = eTimeJavaNative-sTimeJavaNative;
//
//                long sTimeSerial = System.currentTimeMillis();
//                MergeSortSerial.mergeSortSerial(dataset[1]);
//                long eTimeSerial = System.currentTimeMillis();
//                testResults[1][testCounts] = eTimeSerial-sTimeSerial;
//
//
//
//
//                for (int threadCounts = 0; threadCounts < THREAD_COUNTS.length; threadCounts++) {
//
//                    long sTimeParallel = System.currentTimeMillis();
//                    MergeSortSerial.mergeSortParallel(dataset[threadCounts + 2], THREAD_COUNTS[threadCounts]);
//                    long eTimeParallel = System.currentTimeMillis();
//                    testResults[2+threadCounts][testCounts] = eTimeParallel-sTimeParallel;
//
//                }
//
//                // Verify everything is sorted
//                for(int j = 0; i < dataset.length; i++) {
//                    verifyAnswer(dataset[j]);
//                }
//
//            }
//
//        }

        long finalMemory = Runtime.getRuntime().totalMemory();

        printTestResults(testResults);

        System.out.println("Initial Memory:\t" + initialMemory/1048576L + " MB");
        System.out.println("Final Memory:\t" + finalMemory/1048576L + " MB");

    }

    private static void printTestResults(long[][] testResults) {

        System.out.printf("%-25s | %6d | %6d | %s\n", "Java Native Serial", getAverage(testResults[0]),
                getMedian(testResults[0]), Arrays.asList(ArrayUtils.toObject(testResults[0])).toString());

        System.out.printf("%-25s | %6d | %6d | %s\n", "Merge Sort Serial", getAverage(testResults[1]),
                getMedian(testResults[1]), Arrays.asList(ArrayUtils.toObject(testResults[1])).toString());

        for(int i = 2; i < testResults.length; i++) {

            System.out.printf("%-20s %4d | %6d | %6d | %s\n", "Merge Sort Parallel", THREAD_COUNTS[i-2],
                    getAverage(testResults[i]), getMedian(testResults[i]),
                    Arrays.asList(ArrayUtils.toObject(testResults[i])).toString());
        }

    }

    private static int[] generateRandomArray(int arrSize, int range) {

        Random random = new Random();
        int[] randomizedArray = new int[arrSize];

        for(int i = 0; i < arrSize; i++) {
            randomizedArray[i] = random.nextInt(range) - 150000000;
        }

        return randomizedArray;
    }

    private static boolean verifyAnswer(int[] ordered) {

        if(ordered.length == 1  || ordered.length == 0) {
            return true;
        }

        for(int i = 0; i < ordered.length-2; i++) {
            if(ordered[i] > ordered[i+1]) {
                return false;
            }
        }
        return true;
    }

    /**
     * Gets median of an array
     *
     * @param longs s
     * @return
     */
    private static long getMedian(long[]longs) {

        long[] copy = Arrays.copyOf(longs, longs.length);
            Arrays.sort(copy);

        return copy[copy.length / 2];

    }

    private static long getAverage(long[] longs) {
        long sum = 0;
        for (int i = 0; i < longs.length; i++) {
            sum += longs[i];
        }
        return sum/longs.length;
    }

}

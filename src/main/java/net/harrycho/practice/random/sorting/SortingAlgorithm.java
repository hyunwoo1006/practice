package net.harrycho.practice.random.sorting;


public abstract class SortingAlgorithm {

    public abstract  void sort(int[] data);

}

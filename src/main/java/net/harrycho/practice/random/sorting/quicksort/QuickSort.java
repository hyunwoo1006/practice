package net.harrycho.practice.random.sorting.quicksort;

import net.harrycho.practice.random.sorting.SortingAlgorithm;

public class QuickSort extends SortingAlgorithm{

    @Override
    public void sort(int[] data) {
        quickSort(data, 0, data.length-1);
    }

    private void quickSort(int[] data, int low, int high) {
        if(low < high) {
            int pivot = partition(data, low, high);
            quickSort(data, low, pivot-1);
            quickSort(data, pivot+1, high);
        }

    }

    public int partition(int[] data, int low, int high) {

        int pivotValue = data[high];

        int i = low;
        for(int j = low; j <= high-1; j++) {
            if(data[j] <= pivotValue) {
                swap(data, i, j);
                i++;
            }
        }

        swap(data, i, high);
        return i;

    }

    private void swap(int[] data, int i, int j) {
        int temp = data[i];
        data[i] = data[j];
        data[j] = temp;
    }


}

package net.harrycho.practice.random.sorting.comparison;

import net.harrycho.practice.random.sorting.SortingAlgorithm;
import net.harrycho.practice.random.sorting.mergesort.MergeSortParallel;
import net.harrycho.practice.random.sorting.mergesort.MergeSortSerial;
import net.harrycho.practice.random.sorting.quicksort.QuickSort;

import java.util.Arrays;

public class Driver {

    public static void main(String [] args) {

        int[] threadCounts = {2, 4, 8, 16, 32, 64, 128, 256, 512, 1024, 2048, 4096};

        AlgosComparisonTest comparisonTest = new AlgosComparisonTest();
        comparisonTest.addSortingAlgorithm("Serial MergeSort", new MergeSortSerial());
        for(int i = 0; i < threadCounts.length; i++) {
            comparisonTest.addSortingAlgorithm("MergeSort T:" + threadCounts[i],
                    new MergeSortParallel(threadCounts[i]));
        }
        comparisonTest.addSortingAlgorithm("QuickSort", new QuickSort());
        comparisonTest.addSortingAlgorithm("Arrays.sort()", new SortingAlgorithm() {
                    @Override
                    public void sort(int[] data) {
                        Arrays.sort(data);
                    }
                });

        comparisonTest.testAndPrintResult();

    }

}

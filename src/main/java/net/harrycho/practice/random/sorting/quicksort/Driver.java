package net.harrycho.practice.random.sorting.quicksort;

import org.apache.commons.lang3.ArrayUtils;

import java.util.Arrays;

/**
 * Created by root on 12/8/15.
 */
public class Driver {
    public static void main(String [] args) {

        int[] testdata1 = {1, 2, 3, 4, 5};
        int[] testdata2 = {2, 7, 3, 4, 5};
        int[] testdata3 = {3};
        int[] testdata4 = {2, 2, 2, 2, 2, 2, 2, 2, 2};
        int[] testdata5 = {1, 2};
        int[] testdata6 = {2, 1};


        QuickSort qsort = new QuickSort();


//        qsort.quickSort(testdata1);
//        System.out.println(Arrays.asList(ArrayUtils.toObject(testdata1)));
//
//        qsort.quickSort(testdata2);
//        System.out.println(Arrays.asList(ArrayUtils.toObject(testdata2)));
//
//        qsort.quickSort(testdata3);
//        System.out.println(Arrays.asList(ArrayUtils.toObject(testdata3)));
//
//        qsort.quickSort(testdata4);
//        System.out.println(Arrays.asList(ArrayUtils.toObject(testdata4)));


        int p1 = qsort.partition(testdata1, 0, testdata1.length-1);
        printArray(testdata1, p1);

        int p2 = qsort.partition(testdata2, 0, testdata2.length-1);
        printArray(testdata2, p2);

        int p3 = qsort.partition(testdata3, 0, testdata3.length-1);
        printArray(testdata3, p3);

        int p4 = qsort.partition(testdata4, 0, testdata4.length-1);
        printArray(testdata4, p4);

        int p5 = qsort.partition(testdata5, 0, testdata5.length-1);
        printArray(testdata5, p5);

        int p6 = qsort.partition(testdata6, 0, testdata6.length-1);
        printArray(testdata6, p6);


    }


    private static void printArray(int[] arr, int pivot) {
        System.out.println("Pivot=" + pivot + " | " + Arrays.asList(ArrayUtils.toObject(arr)));
    }

}

package net.harrycho.practice.random.sorting.comparison;

import net.harrycho.practice.random.sorting.SortingAlgorithm;
import org.apache.commons.lang3.ArrayUtils;

import java.util.*;


public class AlgosComparisonTest {

    // Default Values
    private int intRange       = 300000000;
    private int arraySize      = 33554432;  // 2^25
    private int testCount      = 5;
    private Map<String, SortingAlgorithm> sortingAlgos;
    private Map<String, List<Long>> testResults;

    public AlgosComparisonTest() {
        sortingAlgos = new LinkedHashMap<String, SortingAlgorithm>();
    }


    public void addSortingAlgorithm(String name, SortingAlgorithm algorithm) {
        if(sortingAlgos.containsKey(name)) {
            throw new IllegalArgumentException("Algorithm=" + name + " already exists.");
        }
        sortingAlgos.put(name, algorithm);
    }

    public Set<String> getAlgorithmNames() {
        return sortingAlgos.keySet();
    }

    public Map<String, SortingAlgorithm> getSortingAlgorithms() {
        return sortingAlgos;
    }

    public void reset() {
        intRange       = 300000000;
        arraySize      = 33554432;  // 2^25
        testCount      = 5;
        sortingAlgos = new LinkedHashMap<String, SortingAlgorithm>();
    }


    public void test() {
        if(sortingAlgos.isEmpty()) {
            throw new IllegalStateException("There is no sorting algorithm to test against.");
        }

        long initialMemory = Runtime.getRuntime().totalMemory();

        // Initialize Test Results Map
        testResults = new LinkedHashMap<String, List<Long>>();
        for(String algoName : sortingAlgos.keySet()) {
            testResults.put(algoName, new ArrayList<Long>());
        }

        for(int i = 0; i < testCount; i++) {


            int datasetIndex = 0;
            for(String algorithmName : sortingAlgos.keySet()) {

                int[] randomInts = generateRandomArray(arraySize, intRange);

                SortingAlgorithm sortingAlgorithm = sortingAlgos.get(algorithmName);
                List<Long> results = testResults.get(algorithmName);

                long startTime = System.currentTimeMillis();
                    sortingAlgorithm.sort(randomInts);
                long endTime = System.currentTimeMillis();

                results.add(endTime-startTime);

                verifyAnswer(randomInts);

                datasetIndex++;

            }

        }

        long finalMemory = Runtime.getRuntime().totalMemory();

        System.out.println("Initial Memory:\t" + initialMemory/1048576L + " MB");
        System.out.println("Final Memory:\t" + finalMemory/1048576L + " MB");

    }

    public void printResult() {
        if(testResults == null) {
            throw new IllegalStateException("Test has not been executed. Call test() first.");
        }

        for(String algoName : testResults.keySet()) {

            List<Long> result = testResults.get(algoName);

            System.out.printf("%-25s | %6d | %6d | %s\n", algoName, getAverage(result),
                    getMedian(result), result.toString());
        }


    }

    public void testAndPrintResult() {
        test();
        printResult();
    }

    public int getIntRange() {
        return intRange;
    }

    public void setIntRange(int intRange) {
        this.intRange = intRange;
    }

    public int getArraySize() {
        return arraySize;
    }

    public void setArraySize(int arraySize) {
        this.arraySize = arraySize;
    }

    public int getTestCount() {
        return testCount;
    }

    public void setTestCount(int testCount) {
        this.testCount = testCount;
    }

    public Map<String, List<Long>> getTestResults() {
        return testResults;
    }

    /**
     * Creates new array of "arrSize" that has random integers ranging from -range to range-1
     *
     * @param arrSize   size of random array
     * @param range     data range (-range to range-1)
     * @return          array populated with random data
     */
    private static int[] generateRandomArray(int arrSize, int range) {

        Random random = new Random();
        int[] randomizedArray = new int[arrSize];

        for(int i = 0; i < arrSize; i++) {
            randomizedArray[i] = random.nextInt(range) - 150000000;
        }

        return randomizedArray;
    }

    /**
     * Verifies if given array is sorted or not.
     *
     * @param ordered   data to test against
     * @return          true if sorted, false if not
     */
    private boolean verifyAnswer(int[] ordered) {

        if(ordered.length == 1  || ordered.length == 0) {
            return true;
        }

        for(int i = 0; i < ordered.length-2; i++) {
            if(ordered[i] > ordered[i+1]) {
                throw new RuntimeException("Algorithm Failed. List is not sorted. Terminating!!");
            }
        }
        return true;
    }

    /**
     * Gets median of an array. Does not modify the data.
     *
     * @param longs s   data to get median of
     * @return          median value of s
     */
    private long getMedian(List<Long> longs) {
        List<Long> copy = new ArrayList<Long>(longs);
        return copy.get(copy.size() / 2);
    }

    /**
     * Gets average of given data
     *
     * @param longs data to get median
     * @return      median from data
     */
    private long getAverage(List<Long> longs) {
        long sum = 0;
        for (int i = 0; i < longs.size(); i++) {
            sum += longs.get(i);
        }
        return sum/longs.size();
    }

}

package net.harrycho.practice.random.sorting.mergesort;

import net.harrycho.practice.random.sorting.SortingAlgorithm;

public class MergeSortSerial extends SortingAlgorithm {

    @Override
    public void sort(int[] data) {

        mergeSortSerial(data, 0, data.length - 1);
    }

    private void mergeSortSerial(int[] data, int left, int right) {

        // When there is 1 element, we stop
        if(right-left < 1) {
            return;
        }

        int middle = left + (right-left) / 2;

        mergeSortSerial(data, left, middle);
        mergeSortSerial(data, middle + 1, right);

        merge(data, left, middle, right);

    }

    private void merge(int[] data, int left, int middle, int right) {

        int[] sorted = new int[right-left+1];

        int sortedIndex = 0;
        int leftIndex = left;
        int rightIndex = middle+1;

        for(int i = 0; i < sorted.length; i++) {
            int leftVal;
            int rightVal;

            if(leftIndex > middle) {
                // Add right
                sorted[sortedIndex] = data[rightIndex];
                rightIndex++;
                sortedIndex++;
                continue;
            }

            if(rightIndex > right) {
                // Add left
                sorted[sortedIndex] = data[leftIndex];
                leftIndex++;
                sortedIndex++;
                continue;
            }

            leftVal = data[leftIndex];
            rightVal = data[rightIndex];

            if(leftVal <= rightVal) {
                sorted[sortedIndex] = data[leftIndex];
                leftIndex++;
            } else {
                sorted[sortedIndex] = data[rightIndex];
                rightIndex++;
            }
            sortedIndex++;
        }

        for(int i = 0; i < sorted.length; i++) {
            data[left] = sorted[i];
            left++;
        }
    }


}

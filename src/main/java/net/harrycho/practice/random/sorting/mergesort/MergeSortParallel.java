package net.harrycho.practice.random.sorting.mergesort;

import net.harrycho.practice.random.sorting.SortingAlgorithm;

public class MergeSortParallel extends SortingAlgorithm {

    private int threadCount = 1;

    public MergeSortParallel(int threadCount) {
        this.threadCount = threadCount;
    }

    @Override
    public void sort(int[] data) {
        mergeSortParallel(data, 0, data.length - 1, threadCount / 2);
    }


    private void mergeSortParallel(int[] data, int left, int right, int threadCount) {

        // When there is 1 element, we stop
        if(right-left < 1) {
            return;
        }

        int middle = left + (right-left) / 2;

        Thread thread = null;

        if (threadCount >= 1) {
            thread = new Thread(new MergeSortTask(data, left, middle, threadCount / 2));
            thread.start();
        } else {
            mergeSortParallel(data, left, middle, threadCount / 2);
        }

        mergeSortParallel(data, middle + 1, right, threadCount / 2);

        if (thread != null) {
            try {
                thread.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        merge(data, left, middle, right);

    }

    private void merge(int[] data, int left, int middle, int right) {

        int[] sorted = new int[right-left+1];

        int sortedIndex = 0;
        int leftIndex = left;
        int rightIndex = middle+1;

        for(int i = 0; i < sorted.length; i++) {
            int leftVal;
            int rightVal;

            if(leftIndex > middle) {
                // Add right
                sorted[sortedIndex] = data[rightIndex];
                rightIndex++;
                sortedIndex++;
                continue;
            }

            if(rightIndex > right) {
                // Add left
                sorted[sortedIndex] = data[leftIndex];
                leftIndex++;
                sortedIndex++;
                continue;
            }

            leftVal = data[leftIndex];
            rightVal = data[rightIndex];

            if(leftVal <= rightVal) {
                sorted[sortedIndex] = data[leftIndex];
                leftIndex++;
            } else {
                sorted[sortedIndex] = data[rightIndex];
                rightIndex++;
            }
            sortedIndex++;
        }

        for(int i = 0; i < sorted.length; i++) {
            data[left] = sorted[i];
            left++;
        }
    }


    private class MergeSortTask implements Runnable{

        private int[] data;
        private int left;
        private int right;
        private int threadCount;

        public MergeSortTask(int[] data, int left, int right, int threadCount) {
            this.data = data;
            this.left = left;
            this.right = right;
            this.threadCount = threadCount;
        }

        public void run() {
            mergeSortParallel(data, left, right, threadCount);
        }

    }

}

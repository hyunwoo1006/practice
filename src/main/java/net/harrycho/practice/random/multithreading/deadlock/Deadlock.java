package net.harrycho.practice.random.multithreading.deadlock;

import java.util.Objects;

/**
 * Deadlock occurs when all following conditions are true
 * 1. Mutual Exclusion
 * 2. Circular Dependency
 * 3. Non-preemption
 * 4. Wait and hold
 *
 */
public class Deadlock {

    private static long PROCESSING_TIME = 1000L;

    private static Object r1 = new Object();
    private static Object r2 = new Object();

    private static class ProcessOne implements Runnable {

        public void run() {
            while (true) {
                synchronized (r1) {
                    try {
                        r1.wait(PROCESSING_TIME);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    synchronized (r2) {
                        try {
                            r2.wait(PROCESSING_TIME);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                    System.out.println("ProcessOne completed");
                }
            }
        }
    }

    private static class ProcessTwo implements Runnable {

        public void run() {
            while(true) {
                synchronized (r2) {
                    try {
                        r2.wait(PROCESSING_TIME);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    synchronized (r1) {
                        try {
                            r1.wait(PROCESSING_TIME);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                }
                System.out.println("ProcessTwo Completed");
            }
        }

    }

    public static void main(String [] args) {
        Thread t1 = new Thread(new ProcessOne());
        Thread t2 = new Thread(new ProcessTwo());
        t1.start();
        t2.start();

    }

}

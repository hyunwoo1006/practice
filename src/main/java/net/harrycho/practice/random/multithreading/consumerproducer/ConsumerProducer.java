package net.harrycho.practice.random.multithreading.consumerproducer;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class ConsumerProducer {

    public static int MAX_SIZE = 30;

    public static void main(String [] args) {
        BlockingBuffer buffer = new BlockingBuffer();

        ConsumerTask consumerTask = new ConsumerTask(buffer);
        ProducerTask producerTask = new ProducerTask(buffer);
        Thread consumer1 = new Thread(consumerTask);
        Thread consumer2 = new Thread(consumerTask);
        Thread producer = new Thread(producerTask);

        consumer1.start();
        consumer2.start();
        producer.start();

    }

}

class BlockingBuffer {
    private List<Integer> buffer = Collections.synchronizedList(new ArrayList<Integer>());

    public synchronized void put(int i) {
        while(buffer.size() > ConsumerProducer.MAX_SIZE) {
            try {
                wait();
            } catch (InterruptedException e) {
//                e.printStackTrace();
            }
        }
        buffer.add(i);
        notifyAll();

    }

    public synchronized int get() {
        while(buffer.size() <= 0) {
            try {
                wait();
            } catch (InterruptedException e) {
//                e.printStackTrace();
            }
        }
        int retVal = buffer.remove(0);
        notifyAll();
        return retVal;
    }


}

class ConsumerTask implements Runnable {

    private BlockingBuffer blockingBuffer;

    public ConsumerTask(BlockingBuffer blockingBuffer){
        this.blockingBuffer = blockingBuffer;
    }


    public void run() {
        while(true) {
            System.out.println(blockingBuffer.get());
        }
    }
}


class ProducerTask implements Runnable {

    private BlockingBuffer blockingBuffer;
    private int i = 0;

    public ProducerTask(BlockingBuffer blockingBuffer){
        this.blockingBuffer = blockingBuffer;
    }

    public void run() {
        while(true) {

            blockingBuffer.put(i++);
        }
    }
}



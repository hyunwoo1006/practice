package net.harrycho.practice.random.datastructure.tree.avl;

import net.harrycho.practice.commons.tree.TreeNode;

public class AVLNode
{

    public int val;
    public int height;
    public AVLNode left;
    public AVLNode right;

    public AVLNode(int val) {
        this.val = val;
    }
}

package net.harrycho.practice.random.datastructure.list.arraylist;


import java.util.Arrays;

/**
 *
 * Simple ArrayList that implements only certain methods:
 *  1. add
 *  2. get
 *  3. remove
 *  4. size
 *
 * This is the reason why I did not implement List interface.
 *
 * @param <E>
 */

/**
 * TODO : Current implementation is very naive.
 *  1. fixed adjustment factor is very bad since adding 100 will require reallocating memory 10 time.
 *          better solution is to have adjustment factor increase exponentially.
 *  2. Calculating size using adjustment factor is also naive.
 *
 *
 * @param <E>
 */
public class MyArrayList<E> {


    private static final int ADJUSTMENT_FACTOR = 10;
    private int increaseFactorBound = 0; // Used for decreasing and increasing the array size

    private Object[] data;


    public int getDataLength() {
        return data.length;
    }

    // Size need to be dynamic.
    // We increase as we need more size; we can give about 10 each
    // Insert MUST be O(1)

    public MyArrayList() {
        data = new Object[ADJUSTMENT_FACTOR];
    }

    public int size() {
        return (data.length/ADJUSTMENT_FACTOR-1)*ADJUSTMENT_FACTOR +increaseFactorBound;
    }

    public void add(E item) {
        if(increaseFactorBound == ADJUSTMENT_FACTOR-1) {
            // current array size is full. we need to increase by the specified factor;
            data = Arrays.copyOf(data, data.length+ ADJUSTMENT_FACTOR);
            increaseFactorBound = 0;
        } else {
            increaseFactorBound++;
        }
        data[size()-1] = item;
    }

    public E remove(int index) {
        if(index < 0 || index > size()-1) {
            throw new IndexOutOfBoundsException("index " + index + " is out of bounds 0-" + (size()-1));
        }
        // delete index and shift.
        E deleted = get(index);
        System.arraycopy(data, index, data, index+1, size()-index-1);
        if(increaseFactorBound == 0) {
            // Reset bound
            data = Arrays.copyOf(data, data.length-ADJUSTMENT_FACTOR);
            increaseFactorBound = ADJUSTMENT_FACTOR -1;
        } else {
            // Decrease size of array
            // decrease bound only
            increaseFactorBound--;
        }


        return deleted;
    }

    @SuppressWarnings("unchecked")
    public E get(int index) {
        if(index < 0 || index > size()-1) {
            throw new IndexOutOfBoundsException("index " + index + " is out of bounds 0-" + (size()-1));
        }
        return (E) data[index];
    }
}

package net.harrycho.practice.random.datastructure.tree.InvertTree;

import net.harrycho.practice.commons.tree.TreeNode;

public class Driver {
    public static void main(String [] args) {
        InvertTreeRecursive invertTreeRecursive = new InvertTreeRecursive();

        // Build tree bottom-up
        TreeNode a10 = new TreeNode(10);
        TreeNode a1 = new TreeNode(1);
        TreeNode a20 = new TreeNode(a10, a1, 20);
        TreeNode a30 = new TreeNode(30);
        TreeNode a60 = new TreeNode(a30, a20, 60);
        TreeNode a40 = new TreeNode(40);
        TreeNode a25 = new TreeNode(25);
        TreeNode a50 = new TreeNode(a40, a25, 50);
        TreeNode aroot = new TreeNode(a60, a50, 90);
        invertTreeRecursive.invertTreeRecursive(aroot);


        System.out.println("Program Terminated");
    }
}

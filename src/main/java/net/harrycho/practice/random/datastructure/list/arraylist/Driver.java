package net.harrycho.practice.random.datastructure.list.arraylist;

import java.util.ArrayList;

public class Driver {


    public static void main(String [] args) {


        System.out.println(Runtime.getRuntime().totalMemory());
        MyArrayList<Integer> arrayList = new MyArrayList<Integer>();


        int testSize = 10000000;
//        for(int i = 1; i <= testSize; i++) {
//
//            arrayList.add(i);
//
//            System.out.println(arrayList.size() + " : " + i + " : " + arrayList.getDataLength());
//            assert arrayList.size() == i;
//
//        }
//
//        for(int i = 0; i < testSize; i++) {
//            System.out.println(arrayList.get(i));
//        }
//
//        for(int i = testSize-1; i >= 0; i--) {
//            int item  = arrayList.remove(i);
//            System.out.println(item + " : " + arrayList.size() + " : " + arrayList.getDataLength());
//
//
//        }


//        ArrayList<Integer> alist = new ArrayList<Integer>();
        for(int i = 0; i < testSize; i++) {
            arrayList.add(i);
        }

        for(int i = arrayList.size()-1; i >= 0; i--) {
            arrayList.remove(i);
        }


        System.out.println(Runtime.getRuntime().totalMemory());
    }

}

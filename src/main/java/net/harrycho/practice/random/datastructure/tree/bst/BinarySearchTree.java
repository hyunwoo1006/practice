package net.harrycho.practice.random.datastructure.tree.bst;

import net.harrycho.practice.commons.tree.TreeNode;
import net.harrycho.practice.topcoder.ABBA.RandomizedTestGenerator;

public class BinarySearchTree {

    private TreeNode root;

    public void add(int i) {
        root = add(i, root);
    }

    private TreeNode add(int i, TreeNode node) {
        if(node == null) {
            node = new TreeNode(i);
        } else if(i < node.value) {
            node.left = add(i, node.left);
        } else if(i > node.value) {
            node.right = add(i, node.right);
        } else {
            // Do nothing, duplicate
        }
        return node;
    }

    public boolean contains(int i) {
        return contains(i, root);
    }

    private boolean contains(int i, TreeNode node) {
        if(node == null) {
            return false;
        } else if(i < node.value) {
            return contains(i, node.left);
        } else if(i > node.value) {
            return contains(i, node.right);
        } else {
            return true;
        }
    }

    public void preOrderTraversal() {

        if(root == null) {
            System.out.println("null root");
        }

        preOrderTraversal(root);
        System.out.println();

    }

    private void preOrderTraversal(TreeNode node) {

        if(node == null) {
            return;
        }

        preOrderTraversal(node.left);
        System.out.print(node.value + " ");
        preOrderTraversal(node.right);

    }

    public void remove(int i) {
        root = remove(i, root);
    }

    public TreeNode remove(int i, TreeNode node) {
        if(node == null) {
            // Node is not found, we do not do anything
        } else if(i < node.value) {
            node.left = remove(i, node.left);
        } else if(i > node.value) {
            node.right = remove(i, node.right);
        } else {
            // Node is found
            if(node.left == null && node.right == null) {
                // Doesn't have any children, simply remove current one
                node = null;
            } else if(node.left != null && node.right != null) {
                // Get min of right subtree
                TreeNode min = getMinNode(node.right);
                node.right = remove(min.value, node.right);
                node.value = min.value;
            } else {
                // Has only one node
                node = node.left != null ? node.left : node.right;
            }

        }
        return node;
    }

    public void removeAll() {
        root = null;
    }

    public int getMin() {
        return getMinNode(root).value;
    }

    private TreeNode getMinNode(TreeNode node) {

        while(node.left != null) {
            node = node.left;
        }

        return node;

    }

    public int getMax() {
        return getMaxNode(root).value;
    }

    public TreeNode getMaxNode(TreeNode node) {
        while(node.right != null) {
            node = node.right;
        }
        return node;
    }

    public int getSecondMin() {
        // Second min is either
        //  1) Parent if min has no right child
        //  2) Right child's min if has right child

        if(root == null || (root.left == null && root.right == null)) {
            throw new IllegalStateException("there is either no root or has only 1 node. Therefore, no 2nd min");
        }


        TreeNode currNode = root;
        TreeNode parent = null;

        while(currNode.left != null) {
            parent = currNode;
            currNode = currNode.left;
        }

        if(currNode.right != null) {
            return getMinNode(currNode.right).value;
        } else {
            return parent.value;
        }

    }


}

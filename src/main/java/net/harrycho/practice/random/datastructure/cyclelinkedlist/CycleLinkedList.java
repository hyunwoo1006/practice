package net.harrycho.practice.random.datastructure.cyclelinkedlist;

import net.harrycho.practice.commons.list.SLNode;

public class CycleLinkedList {

    public static boolean hasCycle(SLNode head) {

        if(head == null || head.next == null) {
            return false;
        }

        SLNode slow = head;
        SLNode fast = head.next;

        while(fast != null) {

            if(slow == fast) {
                return true;
            }

            slow = slow.next;
            if(fast.next == null) {
                return false;
            } else {
                fast = fast.next.next;
            }

        }
        return false;

    }

    public static void main(String [] args) {

        // FALSE
        SLNode test1Head = new SLNode(1);
        System.out.println(hasCycle(test1Head));

        // FALSE
        SLNode test2Head = new SLNode(1);
        SLNode test2node1 = new SLNode(2);
        test2Head.next = test2node1;
        System.out.println(hasCycle(test2Head));

        // TRUE
        SLNode test3head = new SLNode(1);
        SLNode test3node1 = new SLNode(2);
        test3head.next = test3node1;
        test3node1.next = test3head;
        System.out.println(hasCycle(test3head));

        // TRUE
        SLNode test4head = new SLNode(1);
        test4head.next = test4head;
        System.out.println(hasCycle(test4head));

        // TRUE
        SLNode test5head = new SLNode(5);
        SLNode test5node1 = new SLNode(3);
        SLNode test5node2 = new SLNode(4);
        SLNode test5node3 = new SLNode(5);
        test5head.next = test5node1;
        test5node1.next = test5node2;
        test5node2.next = test5node3;
        test5node3.next = test5head;
        System.out.println(hasCycle(test5head));


    }
}

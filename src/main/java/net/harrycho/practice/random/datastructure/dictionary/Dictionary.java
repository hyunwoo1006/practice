package net.harrycho.practice.random.datastructure.dictionary;

public class Dictionary {

    // Root node's value is meaningless
    private DNode root;

    public void addWord(String word) {
        if(word.length() == 0) {
            return;
        }
        if(root == null) {
            root = new DNode();
        }
        addWord(word.toLowerCase(), root);
    }

    private void addWord(String word, DNode node) {
        if(!node.hasChild(word.charAt(0))) {
            DNode newNode = new DNode(word.charAt(0));
            node.children[word.charAt(0)-97] = newNode;
        }
        if(word.length() == 1) {
            node.children[word.charAt(0)-97].isWord = true;
        } else {
            addWord(word.substring(1, word.length()), node.children[word.charAt(0)-97]);
        }
    }

    public boolean search(String word) {
        if(root == null) {
            return false;
        }
        return searchHelper(word, root);
    }

    private boolean searchHelper(String remaining, DNode node) {
        DNode childNode = node.getChild(remaining.charAt(0));
        if (childNode == null) {
            return false;
        } else {
            if(remaining.length() == 1) {
                if(childNode.isWord) {
                    return true;
                } else {
                    return false;
                }
            } else {
                return searchHelper(remaining.substring(1, remaining.length()), childNode);
            }
        }
    }

    public void printAllWords() {
        printAllWordsInNode("", root);
    }

    public void printAllWordsWithPrefix(String prefix) {
        if(root == null) {
            return;
        }
        if(prefix.length() == 0) {
            printAllWords();
        }
        printAllWordsWithPrefix(prefix, prefix, root);
    }

    private void printAllWordsWithPrefix(String prefix, String remaining, DNode node) {
        DNode childNode = node.getChild(remaining.charAt(0));
        if (childNode == null) {
            return;
        } else {
            if(remaining.length() == 1) {
                printAllWordsInNode(prefix, childNode);
            } else {
                printAllWordsWithPrefix(prefix, remaining.substring(1, remaining.length()), childNode);
            }
        }
    }

    private void printAllWordsInNode(String appended, DNode node) {
        for(int i = 0; i < node.children.length; i++) {

            if(node.children[i] != null) {
                if(node.children[i].isWord) {
                    System.out.println(appended + node.children[i].ch);
                } else {
                    printAllWordsInNode(appended + node.children[i].ch, node.children[i]);
                }
            }
        }
    }

}

package net.harrycho.practice.random.datastructure.dictionary;

public class DNode {
    public char ch;
    public boolean isWord;
    public DNode[] children = new DNode[26]; // 26 is total number of alphabets

    public DNode() {}

    public DNode(char ch) {
        this.ch = ch;
    }

    public boolean hasChild(char ch) {
        ch = Character.toLowerCase(ch);
        return children[ch-97] == null ? false : true;
    }


    public DNode getChild(char ch) {
        ch = Character.toLowerCase(ch);
        return children[ch-97];
    }
}

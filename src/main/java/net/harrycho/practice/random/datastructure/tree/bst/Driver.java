package net.harrycho.practice.random.datastructure.tree.bst;

public class Driver {

    public static void main(String [] args) {

        BinarySearchTree bst = new BinarySearchTree();


        int[] testData1 = {5, 2, 7, 1, 4, 3};

        populateTestData(testData1, bst);

        bst.preOrderTraversal();

        assert bst.getMin() == 1;
        assert bst.getSecondMin() == 2;


        bst.removeAll();


        int[] testData2 = {1, 5, 6, 2, 8, 3, 7, 9, 10, -2, 3, 4};

        populateTestData(testData2, bst);

        bst.preOrderTraversal();

        assert bst.getMin() == -2;
        assert bst.getSecondMin() == 1;

        bst.remove(-2);

        bst.preOrderTraversal();

        assert bst.getMin() == 1;
        assert bst.getSecondMin() == 2;
        assert !bst.contains(-2);

        assert bst.contains(1);

        bst.remove(1);
        bst.preOrderTraversal();

        bst.remove(8);
        bst.preOrderTraversal();

        assert !bst.contains(1);
        assert bst.getMin() == 2;
        assert bst.getSecondMin() == 3;

        assert false;

    }


    private static void populateTestData(int[] testData, BinarySearchTree bst) {

        for(int i = 0; i < testData.length; i++) {

            bst.add(testData[i]);

        }




    }

}

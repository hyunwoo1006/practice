package net.harrycho.practice.random.datastructure.tree.avl;

public class AVL {

    private AVLNode root;


    public int getHeight() {
        return getHeight(root);
    }

    private int getHeight(AVLNode node) {

        return getHeight(node, 1);
    }


    private int getHeight(AVLNode node, int height) {

        if(node == null) {
            return height -1;
        } else {
            return Math.max(getHeight(node.left, height+1), getHeight(node.right, height+1));
        }

    }

    public void printTree() {
        if(root == null) {
            System.out.println("Root node is null");
        } else {
            printTree(root);
        }
    }

    private void printTree(AVLNode node) {
        if(node == null) {
            return;
        } else {
            printTree(node.left);
            System.out.println(node.val);
            printTree(node.right);
        }
    }

    public void insert(int i) {

        root = insert(i, root);
    }

    private AVLNode insert(int i, AVLNode node) {

        if(node == null) {
            node = new AVLNode(i);
        } else if(i < node.val) {
            node.left = insert(i, node.left);
            if(getHeight(node.left) - getHeight(node.right) == 2) {
                if(i < node.left.val) {
                    node = rotateRight(node);
                } else {
                    node = rotateLeftThenRight(node);
                }
            }
        } else if(i > node.val) {
            node.right = insert(i, node.right);
            if(getHeight(node.right) - getHeight(node.left) == 2) {
                if(i > node.right.val) {
                    node = rotateLeft(node);
                } else {
                    node = rotateRightThenLeft(node);
                }
            }
        } else {
            // Same value case. Ignore since duplicate is not allowed in search tree.
        }

        node.height = Math.max(getHeight(node.left), getHeight(node.right)) + 1;
        return node;
    }

    private AVLNode rotateLeft(AVLNode t) {
        AVLNode a = t.right;
        t.right = a.left;
        a.left = t;
        a.height = Math.max(getHeight(a.left), getHeight(a.right)) + 1;
        t.height = Math.max(getHeight(t.left), getHeight(t.right)) + 1;
        return a;
    }


    private AVLNode rotateRight(AVLNode t) {
        AVLNode a = t.left;
        t.left = a.right;
        a.right = t;
        a.height = Math.max(getHeight(a.left), getHeight(a.right)) + 1;
        t.height = Math.max(getHeight(t.left), getHeight(t.right)) + 1;
        return a;
    }

    private AVLNode rotateLeftThenRight(AVLNode t) {
        t.left = rotateLeft(t.right);
        return rotateRight(t);
    }


    private AVLNode rotateRightThenLeft(AVLNode t) {
        t.right = rotateRight(t.left);
        return rotateLeft(t);
    }
}


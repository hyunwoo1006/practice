package net.harrycho.practice.random.datastructure.dictionary;

public class Driver {

    public static void main(String [] args)  {
        Dictionary dictionary = new Dictionary();
        dictionary.addWord("hey");
        dictionary.addWord("hello");
        dictionary.addWord("John");
        dictionary.addWord("jane");

        dictionary.printAllWords();

        System.out.println(dictionary.search("h"));
        System.out.println(dictionary.search("hey"));
        System.out.println(dictionary.search("hello"));
        System.out.println(dictionary.search("hell"));
        dictionary.printAllWordsWithPrefix("he");

        dictionary.printAllWordsWithPrefix("ja");

    }
}

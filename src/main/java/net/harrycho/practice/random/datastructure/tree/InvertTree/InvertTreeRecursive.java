package net.harrycho.practice.random.datastructure.tree.InvertTree;

import net.harrycho.practice.commons.tree.TreeNode;

public class InvertTreeRecursive {
    public TreeNode invertTreeRecursive(TreeNode root) {
        invertTreeRecursiveHelper(root);
        return root;
    }

    private void invertTreeRecursiveHelper(TreeNode treeNode) {

        TreeNode temp = treeNode.left;
        treeNode.left = treeNode.right;
        treeNode.right = temp;

        if(treeNode.left != null) {
            invertTreeRecursiveHelper(treeNode.left);
        }

        if(treeNode.right != null) {
            invertTreeRecursiveHelper(treeNode.right);
        }
    }

}

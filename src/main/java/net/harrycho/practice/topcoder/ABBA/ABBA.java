package net.harrycho.practice.topcoder.ABBA;

public class ABBA {

    private static int numCalls = 0;

    public String canObtain(String initial, String target) {
        if(recurseObtain(initial, target)) {
            return "Possible";
        } else {
            return "Impossible";
        }
    }

    public boolean recurseObtain(String transformed, String target) {

        numCalls++;

        if(transformed.length() > target.length()) {
            return false;
        } else if( !target.contains(transformed) && !new StringBuilder(target).reverse().toString().contains(transformed)) {
            return false;
        } else {
            if(transformed.equals(target)) {
                return true;
            } else {
                return recurseObtain(moveOne(transformed), target) || recurseObtain(moveTwo(transformed), target);
            }
        }
    }

    private String moveOne(String str) {
        return new StringBuilder(str).append('A').toString();
    }

    private String moveTwo(String str) {
        return new StringBuilder(str).reverse().append('B').toString();
    }

    public static void main(String [] args) {
        ABBA abba = new ABBA();
        RandomizedTestGenerator generator = new RandomizedTestGenerator();


        int testCount = 20000;

        RandomizedTestGenerator.Pair<String, String>[] pairs = generator.generateInput(testCount);



        for(int i = 0; i < testCount; i++) {

            String initial = pairs[i].getLeft();
            String expected = pairs[i].getRight();

            String result = abba.canObtain(initial, expected);

            if(result.equalsIgnoreCase("impossible")) {
                System.out.println("WRONG ANSWER. Input=" + initial + " Output=" + expected);
            } else {
                System.out.println("PASSED, Input=" + initial + " Output=" + expected);
            }

            if((i + 1) % 10 == 0) {
                System.out.println(i+1);
            }



        }
    }



}

package net.harrycho.practice.leetcode.validparentheses;

import java.util.ArrayList;
import java.util.List;

public class ValidParentheses {

    public static boolean isValid(String s) {

        if(s == null || s.length() == 0) {
            return false;
        }

        List<Character> seen = new ArrayList<Character>();
        if(isOpening(s.charAt(0))) {
            seen.add(s.charAt(0));
        } else {
            return false;
        }

        for(int i = 1; i < s.length(); i++) {

            if(isOpening(s.charAt(i))) {
                seen.add(0, s.charAt(i));
            } else if(seen.isEmpty())  {
                return false;
            } else if(isClosing(s.charAt(i))) {
                if(getOpeningPair(s.charAt(i)) == seen.get(0)) {
                    seen.remove(0);
                } else {
                    return false;
                }
            } else {
                return false;
            }
        }

        return seen.isEmpty();
    }

    private static char getOpeningPair(char closing) {

        if(closing == ')') {
            return '(';
        } else if(closing == '}') {
            return '{';
        } else {
            return '[';
        }

    }

    private static boolean isOpening(char ch) {
        if(ch == '(' || ch == '{' || ch == '[') {
            return true;
        } else {
            return false;
        }
    }

    private static boolean isClosing(char ch) {
        if(ch == ')' || ch == '}' || ch == ']') {
            return true;
        } else {
            return false;
        }
    }

    public static void main(String [] args) {
        System.out.println(isValid("()[]{}"));
        System.out.println(isValid("(((()))){}"));
        System.out.println(isValid("[])"));
    }
}

package net.harrycho.practice.leetcode.minimumpathsum;

/**
 * Created by root on 12/14/15.
 */
public class MinimumPathSum {

    public static int minPathSum(int[][] grid) {

        int[][] table = new int[grid.length][grid[0].length];

        table[0][0] = grid[0][0];

        for(int i = 1; i < grid.length; i++) {
            table[i][0] = table[i-1][0] + grid[i][0];
        }

        for(int i = 1; i < grid[0].length; i++) {

            table[0][i] = table[0][i-1] + grid[0][i];
        }

        for(int i = 1; i < grid.length; i++) {
            for(int j = 1; j < grid[0].length; j++) {

                table[i][j] = Math.min(table[i][j-1] + grid[i][j], table[i-1][j] + grid[i][j]);


            }
        }

        return table[grid.length-1][grid[0].length-1];

    }

    public static void main(String [] args) {

        int[][] grid1 = {{1, 0}, {0, 1}};
        int[][] grid2 = {{0, 1, 8}, {2, 3, 7}, {4, 5, 6}};
        int[][] grid3 = {{1,2}, {5,6}, {1,1}};

        System.out.println(minPathSum(grid1));
        System.out.println(minPathSum(grid2));
        System.out.println(minPathSum(grid3));


    }



}

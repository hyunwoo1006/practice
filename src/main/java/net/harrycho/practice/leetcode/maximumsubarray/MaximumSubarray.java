package net.harrycho.practice.leetcode.maximumsubarray;

public class MaximumSubarray {


    /**
     * Hmmm interesting question...
     *
     * If the ranges are to be found, i think 2D table is needed to find one which yields O(n^2)
     * But if just max is needed, than can be done in O(n)
     *
     *
     *
     * @param nums
     * @return
     */

    public static int maxSubArray(int[] nums) {


        int sum = nums[0];
        int max = nums[0];

        for(int i = 1; i < nums.length; i++) {

            sum = Math.max(nums[i], sum + nums[i]);
            max = Math.max(max, sum);


        }

        return max;
    }


    public static void main(String [] args) {



        int[] testdata1 = {-2, 1, -3, 4, -1, 2, 1, -5, 4};
        int[] testdata2 = {-2, -3, 4, -1, -2, 1, 5, -3};
        int[] testdata3 = {3};

        System.out.println(maxSubArray(testdata1));
        System.out.println(maxSubArray(testdata2));
        System.out.println(maxSubArray(testdata3));




    }
}

package net.harrycho.practice.leetcode.groupanagrams;

import java.util.*;

public class GroupAnagrams {

    public static List<List<String>> groupAnagrams(String[] strs) {

        Map<String, Integer> indexKeyByHash = new HashMap<String, Integer>();

        Arrays.sort(strs);

        List<List<String>> retVal = new ArrayList<List<String>>();

        for(int i = 0; i < strs.length; i++) {
            String sorted = sorted(strs[i]);
            Integer index = indexKeyByHash.get(sorted);

            if(index == null) {
                List<String> alist = new ArrayList<String>();
                alist.add(strs[i]);
                retVal.add(alist);
                indexKeyByHash.put(sorted, retVal.indexOf(alist));
            } else {
                retVal.get(index).add(strs[i]);
            }
        }

        return retVal;

    }

    public static String sorted(String item) {
        char[] c = item.toCharArray();
        Arrays.sort(c);
        return new String(c);
    }

    public static void main(String [] args) {

        String[] testdata1 = {"eat", "tea", "tan", "ate", "nat", "bat"};

        printResult(groupAnagrams(testdata1));


    }


    private static void printResult(List<List<String>> result) {
        for(int i = 0; i < result.size(); i++) {
            System.out.println(result.get(i));
        }
    }
}

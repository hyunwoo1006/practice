package net.harrycho.practice.leetcode.searchinsertposition;

import org.apache.commons.lang3.ArrayUtils;

import java.util.Arrays;

public class SearchInsertPosition {

    public static int searchInsert(int[] nums, int target) {

        int i = 0;
        while(i < nums.length) {

            if(nums[i] > target || nums[i] == target) {
                return i;
            }

            i++;
        }

        return i;


    }


    private static void printResult(int[] nums, int target, int expected) {
        int insertIndex = searchInsert(nums, target);


        System.out.println((expected == insertIndex ? "O" : "X") + " | " +
                Arrays.asList(ArrayUtils.toObject(nums)) + " | Found=" + insertIndex + " | Expected=" + expected);



    }

    public static void main(String [] args) {



        int[] testdata1 = {1, 3, 5, 6};
        printResult(testdata1, 5, 2);
        printResult(testdata1, 2, 1);
        printResult(testdata1, 7, 4);
        printResult(testdata1, 0, 0);


    }
}

package net.harrycho.practice.leetcode.missingnumber;

public class MissingNumber {


    public static int missingNumber(int[] nums) {



        int max = -1;
        int sum = 0;

        boolean hasZero = false;

        for(int i = 0; i < nums.length; i++) {

            sum += nums[i];

            if(nums[i] > max) {
                max = nums[i];
            }

            if(nums[i] == 0) {
                hasZero = true;
            }


        }

        int expectedSum = findSum(max);

        if(expectedSum - sum == 0) {
            if(hasZero) {
                return max + 1;
            } else {
                return 0;
            }
        }

        return expectedSum - sum;




    }

    private static int findSum(int i) {

        int sum;

        if(i % 2 == 0) {
            // even
            sum = (i + 1) * (i / 2);


        } else {
            // odd

            sum = (i / 2 + 1) * i;
        }

        return sum;


    }

    public static void main(String [] args) {


        int[] testdata1 = {0, 1, 3}; // 2
        System.out.println(missingNumber(testdata1));

        int[] testdata2 = {0, 1, 2, 3, 4, 5, 6, 8}; // 7
        System.out.println(missingNumber(testdata2));

        int[] testdata3 = {1, 2}; // 0
        System.out.println(missingNumber(testdata3));

        int[] testdata4 = {0}; // 1
        System.out.println(missingNumber(testdata4));

        int[] testdata5 = {0, 1}; // 2
        System.out.println(missingNumber(testdata5));


    }

}

package net.harrycho.practice.leetcode.sortedarraytobst;

import net.harrycho.practice.commons.tree.TreeNode;

public class SortedArrayToBST {

    public static TreeNode sortedArrayToBST(int[] nums) {

        if(nums == null || nums.length == 0) {
            return null;
        }

        return constructBST(nums, 0, nums.length-1);
    }


    private static TreeNode constructBST(int[] nums, int left, int right) {

        if(left > right) {
            return null;
        }

        int middle = left + (right - left) / 2;

        TreeNode parent = new TreeNode(nums[middle]);

        parent.left = constructBST(nums, left, middle-1);
        parent.right = constructBST(nums, middle+1, right);

        return parent;

    }

    public static void main(String [] args) {

        int[] testdata1 = {1, 2, 3, 4, 5, 6};
        TreeNode treeNode = sortedArrayToBST(testdata1);


    }



}

package net.harrycho.practice.leetcode.generateparentheses;

import java.util.ArrayList;
import java.util.List;

public class GenerateParentheses {


    public static List<String> generateParenthesis(int n) {
        List<String> retVal = new ArrayList<String>();
        generate(retVal, "", n, n);

        return retVal;
    }



    private static void generate(List<String> retVal, String s, int l, int r) {

        if(l == 0 && r == 0) {
            retVal.add(s);
            return;
        }

        if(l > 0) {
            generate(retVal, s + "(", l-1, r);
        }
        if(r > l) {
            generate(retVal, s + ")", l, r-1);
        }

    }


    public static void main(String [] args) {

        System.out.println(generateParenthesis(3));
    }
}

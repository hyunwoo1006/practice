package net.harrycho.practice.leetcode.largestnumber;

import java.util.*;

public class LargestNumber {

    public static String largestNumber(int[] nums) {

        List<String> list = new ArrayList<String>();

        for(int i = 0; i < nums.length; i++) {
            list.add(nums[i]+"");
        }

        Collections.sort(list, new Comparator<String>() {
            public int compare(String o1, String o2) {
                return (o2 + "" + o1).compareTo(o1 + "" + o2);
            }
        });

        String retVal = "";

        if(list.get(0).equals("0")) {
            return "0";
        }

        for(int i = 0; i < list.size(); i++) {
            retVal += list.get(i);
        }
        return retVal;
    }

//    public static String largestNumber(int[] nums) {
//
//        List<String> list = new ArrayList<String>();
//
//        for(int i = 0; i < nums.length; i++) {
//            list.add(nums[i]+"");
//        }
//
//        Collections.sort(list, new Comparator<String>() {
//            public int compare(String o1, String o2) {
//
//                char[] val1 = o1.toCharArray();
//                char[] val2 = o2.toCharArray();
//
//                int size = (int) Math.max(val1.length, val2.length);
//
//
//
//                for(int i = 0; i < size; i++) {
//                    char inta = 99;
//                    char intb = 99;
//
//                    if(i < val1.length) {
//                        inta = val1[i];
//                    }
//
//                    if(i < val2.length) {
//                        intb = val2[i];
//                    }
//
//                    if(inta < intb) {
//                        return 1;
//                    }
//                    if(inta > intb) {
//                        return -1;
//                    }
//
//                }
//                return 0;
//
//            }
//        });
//
//        String retVal = "";
//
//        if(list.get(0).equals("0")) {
//            return "0";
//        }
//
//
//        for(int i = 0; i < list.size(); i++) {
//            retVal += list.get(i);
//        }
//        return retVal;
//    }


    public static void main(String [] args) {
        int[] testData1 = {8247,824};
        System.out.println(largestNumber(testData1));

        int[] testData2 = {824,938,1399,5607,6973,5703,9609,4398,8247};
        System.out.println(largestNumber(testData2));

        int[] testData3 = {0, 0};
        System.out.println(largestNumber(testData3));

        int[] testData4 = {0, 0, 0, 9};
        System.out.println(largestNumber(testData4));
    }


}

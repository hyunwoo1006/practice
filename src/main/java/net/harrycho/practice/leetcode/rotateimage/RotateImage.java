package net.harrycho.practice.leetcode.rotateimage;

import org.apache.commons.lang3.ArrayUtils;

import java.util.Arrays;

public class RotateImage {
    public static void rotate(int[][] matrix) {

        for(int i = 0; i < matrix.length/2; i++) {


            for(int j = 0; j <  matrix.length-(2*i)-1; j++) {

                // a <-> m
                swap(matrix, i, i+j, matrix.length-1-i-j, i);

                // m <-> p
                swap(matrix, matrix.length-1-i-j, i, matrix.length-1-i, matrix.length-1-i-j);

                // p <-> d
                swap(matrix, matrix.length-1-i, matrix.length-1-i-j, i+j, matrix.length-1-i);


            }
        }


    }

    private static void swap(int[][] matrix, int a1, int b1, int a2, int b2) {
        int temp = matrix[a1][b1];
        matrix[a1][b1] = matrix[a2][b2];
        matrix[a2][b2] = temp;

    }


    public static void main(String [] args) {

        int[][] testData1 = {{1, 2},
                            {3, 4}};
        printMatrix(testData1);
        rotate(testData1);
        printMatrix(testData1);



        int[][] testData2 = {{1, 2, 3},
                            {4, 5, 6},
                            {7, 8, 9}};
        printMatrix(testData2);
        rotate(testData2);
        printMatrix(testData2);


        int[][] testData3 = {{10, 11, 12, 13},
                             {14, 15, 16, 17},
                             {18, 19, 20, 21},
                             {22, 23, 24, 25}};
        printMatrix(testData3);
        rotate(testData3);
        printMatrix(testData3);


        int[][] testData4 = {{10, 11, 12, 13, 14},
                             {15, 16, 17, 18, 19},
                             {20, 21, 22, 23, 24},
                             {25, 26, 27, 28, 29},
                             {30, 31, 32, 33, 34}};
        printMatrix(testData4);
        rotate(testData4);
        printMatrix(testData4);



        int[][] testData6 = {{10, 11, 12, 13, 14, 15},
                             {16, 17, 18, 19, 20 ,21},
                             {22, 23, 24, 25, 26 ,27},
                             {28, 29, 30, 31, 32 ,33},
                             {34, 35, 36, 37, 38 ,39},
                             {40, 41, 42, 43, 44, 45}};
        printMatrix(testData6);
        rotate(testData6);
        printMatrix(testData6);

    }

    private static void printMatrix(int[][] data) {

        for(int i = 0; i < data.length; i++) {
            System.out.println(Arrays.asList(ArrayUtils.toObject(data[i])));
        }
        System.out.println();

    }

}

package net.harrycho.practice.geeksforgeeks.heap.VerifyHeap;

/*
    Heap needs to obey two properties
    1) Child node must be greater/less than parent
    2) Tree need to be balanced

 */

import net.harrycho.practice.commons.tree.TreeNode;

import java.util.LinkedList;
import java.util.Queue;

public class VerifyHeap {

    public boolean verifyHeap(TreeNode treeNode) {

        if(treeNode == null) {
            return false;
        }

        Queue<TreeNode> q = new LinkedList<TreeNode>();
        q.add(treeNode);

        while(q.isEmpty() == false) {
            int qSize = q.size();
            boolean expectingNull = false;
            for(int i = 0; i < qSize; i++) {

                TreeNode currTreeNode = q.remove();
                int currValue = currTreeNode.value;

                // Checking heap property
                if(currTreeNode.left != null) {

                    // Check heap property
                    if(currValue < currTreeNode.left.value) {
                        System.out.println("Rule #1 Violation!");
                        return false;
                    } else {
                        q.offer(currTreeNode.left);
                    }

                    if(i == 0) {
                        expectingNull = false;
                    }

                    if(expectingNull) {
                        // Previous is null but current is not null
                        System.out.println("Rule #2 Violation!");
                        return false;
                    } else {
                        // Previous is not null and current is not null
                        // Do nothing
                    }

                } else {
                    if(i == 0) {
                        expectingNull = true;
                    }

                    if(expectingNull) {
                        // Previous is null and current is null
                        // Do nothing
                    } else {
                        // Previous is not null but current is null
                        expectingNull = true;
                    }
                }

                if(currTreeNode.right != null) {

                    // Check heap property
                    if(currValue < currTreeNode.right.value) {
                        System.out.println("Rule #1 Violation!");
                        return false;
                    } else {
                        q.offer(currTreeNode.right);
                    }

                    if(expectingNull) {
                        // Previous is null but current is not null
                        System.out.println("Rule #2 Violation!");
                        return false;
                    } else {
                        // Previous is not null and current is not null
                        // Do nothing;
                    }

                } else {

                    if(expectingNull) {
                        // Previous is null and current is null
                        // Do nothing
                    } else {
                        // Previous is not null but current is null
                        expectingNull = true;
                    }

                }
            }

            if(expectingNull && !q.isEmpty()) {
                // This case is where complete tree stops.
                // If next level exists then it is not a complete tree.
                while(!q.isEmpty()) {
                    TreeNode n = q.remove();
                    if(n.left != null | n.right != null) {
                        System.out.println("Rule #2 Violation! - Corner case");
                        return false;
                    }
                }
            }


        }

        return true;
    }

}

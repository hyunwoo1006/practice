package net.harrycho.practice.geeksforgeeks.heap.VerifyHeap;

import net.harrycho.practice.commons.tree.TreeNode;

public class Driver {

    public static void main(String [] args) {

        VerifyHeap verifyHeap = new VerifyHeap();

        // Build tree bottom-up
        TreeNode a10 = new TreeNode(10);
        TreeNode a1 = new TreeNode(1);
        TreeNode a20 = new TreeNode(a10, a1, 20);
        TreeNode a30 = new TreeNode(30);
        TreeNode a60 = new TreeNode(a30, a20, 60);
        TreeNode a40 = new TreeNode(40);
        TreeNode a25 = new TreeNode(25);
        TreeNode a50 = new TreeNode(a40, a25, 50);
        TreeNode a90 = new TreeNode(a60, a50, 90);
        printOutput(false, verifyHeap.verifyHeap(a90));

        TreeNode b10 = new TreeNode(10);
        TreeNode b1 = new TreeNode(1);
        TreeNode b30 = new TreeNode(b10, b1, 30);
        TreeNode b20 = new TreeNode(20);
        TreeNode b60 = new TreeNode(b30, b20, 60);
        TreeNode b40 = new TreeNode(40);
        TreeNode b25 = new TreeNode(25);
        TreeNode b50 = new TreeNode(b40, b25, 50);
        TreeNode b90 = new TreeNode(b60, b50, 90);
        printOutput(true, verifyHeap.verifyHeap(b90));

        TreeNode c5 = new TreeNode(5);
        TreeNode c10 = new TreeNode(c5, null, 10);
        printOutput(true, verifyHeap.verifyHeap(c10));

        TreeNode d5 = new TreeNode(5);
        TreeNode d10 = new TreeNode(null, d5, 10);
        printOutput(false, verifyHeap.verifyHeap(d10));

        TreeNode e3 = new TreeNode(3);
        TreeNode e6 = new TreeNode(6);
        TreeNode e5 = new TreeNode(e3, e6, 5);
        TreeNode e10 = new TreeNode(e5, null, 10);
        printOutput(false, verifyHeap.verifyHeap(e10));

        TreeNode f3 = new TreeNode(3);
        TreeNode f4 = new TreeNode(4);
        TreeNode f5 = new TreeNode(f3, f4, 5);
        TreeNode f10 = new TreeNode(f5, null, 10);
        printOutput(false, verifyHeap.verifyHeap(f10));


    }

    public static void printOutput(boolean expected, boolean actual) {

        String correctness = expected==actual ? "O" : "X";

        System.out.println(correctness + " | " + "Expected=" + expected + "\t|\tActual=" + actual + '\n');
    }

}

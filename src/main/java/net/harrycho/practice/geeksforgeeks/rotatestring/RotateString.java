package net.harrycho.practice.geeksforgeeks.rotatestring;

import org.apache.commons.lang3.ArrayUtils;

import java.util.Arrays;

public class RotateString {

    public static void rotateStringInMemory(int[] data, int offset) {

        for(int i = 0; i < data.length-1; i++) {
            swap(data, i, (i+offset)%data.length);
        }

    }

    private static void move(int[] data, int i , int j) {

        int temp = data[i];
        data[i] = data[j];
        data[j] = temp;

    }

    private static void swap(int[] data, int i , int j) {

        int temp = data[i];
        data[i] = data[j];
        data[j] = temp;

    }

    public static void main(String [] args) {

        int[] testdata1 = {1, 2, 3};
        rotateStringInMemory(testdata1, 3);
        System.out.println(Arrays.asList(ArrayUtils.toObject(testdata1)));



    }

}
